#include "BurstManager.h"

BurstManager::BurstManager(DXGraphics& gfx):
    gfx(gfx)
{}

void BurstManager::UpdateBursts(float dt, DXGraphics& gfx)
{
    for (auto& b : bursts)
    {
        b.lifeTime -= dt * decaySpeed;
        b.burst.UpdatePosition(gfx, dt, b.dispFun);
    }
    bursts.erase(std::remove_if(bursts.begin(), bursts.end(), [](auto& b) {return b.lifeTime < 0.0f; }),bursts.end());
}

void BurstManager::SubmitBursts(FrameCommander& fc)
{
    for (auto& b : bursts)
    {
        if (b.type == BurstManager::Type::Particle)
        {
            b.burst.Submit(fc, Techniques::White);          //names are unrelated. bad design
        }
        else
        {
            b.burst.Submit(fc, Techniques::Lighted);        //names are unrelated. bad design
        }
    }
}

void BurstManager::InitiateBurstFromTrail(const Bind::VertexBuffer& vb, BurstManager::Type type)
{
    auto nVertices = vb.vbuf.Size();
    float* data = (float*)vb.vbuf.GetData();
    std::vector<DirectX::XMFLOAT3> particles;
    for (size_t i = 0u; i < nVertices; i++)
    {
        particles.emplace_back(data[6 * i], data[6 * i + 1], data[6 * i + 2] );     //6 - because there are 6 floats in a vetex
    }
    bursts.emplace_back(ParticleBurst{ gfx, std::move(particles) }, type);
}

namespace ParticleDespersionFunctions
{
    Vec3 DispersionFun(Vec3 pos)                //particle only
    {
        return Vec3{  tan(pos.x)* sin(pos.y), 0.01f * cos(pos.y) / pos.z, cos(pos.x) - cos(pos.y) + sin(pos.z) }*moveIntencity;
    }
    Vec3 AwayFun(Vec3 pos)
    {
        return Vec3(pos.x*cos(pos.x),pos.y*sin(pos.y),pos.z*tan(pos.z)) * moveIntencity;
    }
    Vec3 ImplotionFun(Vec3 pos)
    {
        return Vec3(-pos.x  + 0.5f*pos.y, -pos.y*1.5f, -pos.z-0.5f*pos.x) * moveIntencity;
    }
    Vec3 TwisterFun(Vec3 pos)
    {
        return Vec3(0.2f*sin(pos.x)*pos.y, 0.2f * sin(pos.y)+pos.z, 0.2f * sin(pos.z)*tan(pos.x)) * moveIntencity;
    }
    Vec3 VortexFun(Vec3 pos)
    {
        return Vec3(-pos.x *2.0f * cos(pos.z) + sin(pos.y)* tan(pos.z), -pos.y + cos(pos.z), -pos.z + sin(pos.x)) * moveIntencity;
    }
    Vec3 ScatterFun(Vec3 pos)
    {
        return Vec3(pos.y * tan(pos.x), cos(pos.z + pos.y) * pos.x,pos.x*pos.y*pos.z) * moveIntencity;
    }
    Vec3 StretchFun(Vec3 pos)
    {
        return Vec3(0.1f,cos(pos.x),sin(pos.x)) * moveIntencity;
    }
}

BurstManager::BurstData::BurstData(ParticleBurst&& b, BurstManager::Type type):
    burst(std::move(b)),
    type(type)
{
    if (type == BurstManager::Type::Particle)
    {
        int i = RU.GetInt(0, 4);
        switch (i)
        {
        case 0:
            dispFun = ParticleDespersionFunctions::DispersionFun;
            break;
        case 1:
            dispFun = ParticleDespersionFunctions::VortexFun;
            break;
        case 2:
            dispFun = ParticleDespersionFunctions::ScatterFun;
            break;
        case 3:
            dispFun = ParticleDespersionFunctions::TwisterFun;
            break;
        case 4:
            dispFun = ParticleDespersionFunctions::AwayFun;
            break;
        default:
            break;
        }
    }
    else
    {
        int i = RU.GetInt(0, 1);
        switch (i)
        {
        case 0:
            dispFun = ParticleDespersionFunctions::StretchFun;
            break;
        case 1:
            dispFun = ParticleDespersionFunctions::ImplotionFun;
            break;
        default:
            break;
        }
    
    }
}
