#include "Structures.h"
#include <fstream>
#include <iomanip>


std::ostream& operator<<(std::ostream& os, const handParam& obj)
{
	os << std::setprecision(8)<< obj.asc << std::endl;
	os << std::setprecision(8)<< obj.rot << std::endl;
	os << std::setprecision(8)<< obj.pit << std::endl;
	os << obj.r << std::endl;
	os << obj.vasc << std::endl;
	os << obj.vrot << std::endl;
	os << obj.vpit << std::endl;
	return os;
}