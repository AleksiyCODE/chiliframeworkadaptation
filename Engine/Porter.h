#pragma once
#include <string>
#include <vector>
#include "Structures.h"
class Porter		//used for importing and exporting 
{
public:
	enum class Result
	{
		LoadSuccess,
		LoadFail,
		Idle
	};
	Porter(ComputingMode& comp, Techniques& tec, bool& cum, std::vector<handParam>& handsData);	//none const refs to allow load function to change values directly
	Result SpawnWindow(); //returns 1 if bad data vas inserted and hands need to be reset
private:
	void ParceFile();
	bool LoadParam(size_t position);		//returns 1 if bad data vas inserted and hands need to be reset
	bool SaveParameters(const std::string& exportName);
	char entryBegin[8] = "##$EP%%";
	char entryEnd[8] = "**EN0^^";
	char handEnd[8] = "DaHanIA";
	bool CheckCurrentData();
	void RestoreZeroData();
	//void ParceFile
	std::vector<std::string> loadablePresets;
	char inputBuffer[20] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";		//empty buffer
	bool displaySuccessButton = false;
	bool displayFailButton = false;
	std::string failButtonText;


	static constexpr size_t maxHands = 10u;
	static constexpr size_t minHands = 2u;
	static constexpr float minReasonable = -15.0f;
	static constexpr float maxReasonable = 15.0f;

	ComputingMode& comp;
	Techniques& tec;
	bool& cum;
	std::vector<handParam>& handsData;
};

