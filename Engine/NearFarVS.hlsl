#include "Shaders/Transform.hlsl"
#include "Shaders/Scale.hlsl"
struct VSOut
{
    float closeness : Closeness; //0 - closest to camera; 1 - farthest
    float4 SVpos : SV_Position;
};

cbuffer Distances : register(b2)
{
    float4 distanceToNearest; //from camera to nearest vertex
    float4 distanceToFarthest;
    float4 CameraPos;
};

VSOut main(float3 pos : Position)
{
    VSOut vso;
#ifdef SCALE
    pos*=scale;
#endif    
    float3 dif = pos - CameraPos.xyz;
    float distance = sqrt(dif.x * dif.x + dif.y * dif.y + dif.z * dif.z);
    vso.closeness = abs((distance - distanceToNearest.x) / (distanceToFarthest.x - distanceToNearest.x));
    vso.SVpos = mul(float4(pos, 1.0f), modelViewProj);
    return vso;
}