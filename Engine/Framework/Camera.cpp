

//coded by Chili
#include "Camera.h"
#include "../imgui/imgui.h"
#include "ChiliMath.h"

namespace dx = DirectX;

Camera::Camera() noexcept
{
	Reset();
}

DirectX::XMMATRIX Camera::GetMatrix() const noexcept
{

	using namespace dx;
	const dx::XMVECTOR forwardBaseVector = XMVectorSet(forwardVector.x, forwardVector.y, forwardVector.z, 0.0f);
	// apply the camera rotations to a base vector
	const auto lookVector = XMVector3Transform(forwardBaseVector,
		XMMatrixRotationRollPitchYaw(actual.tilt.roll, actual.tilt.pitch, actual.tilt.yaw)
	);
	// generate camera transform (applied to all objects to arrange them relative
	// to camera position/orientation in world) from cam position and direction
	// camera "top" always faces towards +Y (cannot do a barrel roll)
	const auto camPosition = XMLoadFloat3(&DirectX::XMFLOAT3{ actual.pos.x, actual.pos.y, actual.pos.z });
	const auto camTarget = camPosition + lookVector;
	return XMMatrixLookAtLH(camPosition, camTarget, XMVectorSet(normalVector.x, normalVector.y, normalVector.z, 0.0f));
}

DirectX::XMFLOAT3 Camera::GetPosition() const noexcept
{
	return DirectX::XMFLOAT3{ actual.pos.x, actual.pos.y, actual.pos.z };
}

void Camera::SpawnControlWindow() noexcept
{
	if (ImGui::Begin("Camera"))
	{
		ImGui::Text("Position");
		ImGui::SliderFloat("X", &actual.pos.x, -8.0f, 8.0f, "%.1f");
		ImGui::SliderFloat("Y", &actual.pos.y, -8.0f, 8.0f, "%.1f");
		ImGui::SliderFloat("Z", &actual.pos.z, -8.0f, 8.0f, "%.1f");
		ImGui::Text("Orientation");
		ImGui::SliderAngle("Roll", &actual.tilt.roll, -180.0f, 180.0f);
		ImGui::SliderAngle("Pitch", &actual.tilt.pitch, -90.0f, 90.0f);
		if (ImGui::Button("Reset"))
		{
			Reset();
		}
		ImGui::SliderFloat("forw x", &forwardVector.x, -1.0f, 1.0f, "%.1f");
		ImGui::SliderFloat("forw y", &forwardVector.y, -1.0f, 1.0f, "%.1f");
		ImGui::SliderFloat("forw z", &forwardVector.z, -1.0f, 1.0f, "%.1f");

		ImGui::SliderFloat("UP x", &normalVector.x, -1.0f, 1.0f, "%.1f");
		ImGui::SliderFloat("UP y", &normalVector.y, -1.0f, 1.0f, "%.1f");
		ImGui::SliderFloat("UP z", &normalVector.z, -1.0f, 1.0f, "%.1f");

	}
	ImGui::End();
}

void Camera::Update() noexcept
{
	if (target.tilt.pitch > PI * 0.4 && actual.tilt.pitch < -PI * 0.4)			//if the angle was wraped
	{
		CamTrans temp = target;
		temp.tilt.pitch -= 2 * PI;
		actual = actual * dragCoeff + temp * (1 - dragCoeff);
		actual.tilt.pitch += 2 * PI;
	}
	else
		if (target.tilt.pitch < -PI * 0.4 && actual.tilt.pitch > PI * 0.4)
		{
			CamTrans temp = target;
			temp.tilt.pitch += 2 * PI;
			actual = actual * dragCoeff + temp * (1 - dragCoeff);
			actual.tilt.pitch -= 2 * PI;
		}
		else
		{
			actual = actual * dragCoeff + target * (1 - dragCoeff);
		}
}

void Camera::Reset() noexcept
{
	//pos = { -4.0f,-1.0f,4.0f };
	//roll = 0.0f;
	//pitch = 3.0f * PI / 4.0f;
	//yaw = 0.0f;
	forwardVector = defaultForward;
	normalVector = defaultUp;
}

void Camera::SetFullTransform(CamTrans in_trans)
{
	actual = in_trans;
}

void Camera::Rotate(float dx, float dy) noexcept
{
	actual.tilt.roll = std::clamp(actual.tilt.roll + dy * rotationSpeed, 0.995f * -PI / 2.0f, 0.995f * PI / 2.0f);
	actual.tilt.pitch = wrap_angle(actual.tilt.pitch + dx * rotationSpeed);
}

void Camera::SphereSlide(float dx, float dy) noexcept
{
	SphericMoveUpDown(dy * sphericRotationSpeed);
	SphericRotate(dx * sphericRotationSpeed);
}

void Camera::Translate(DirectX::XMFLOAT3 translation) noexcept
{
	dx::XMStoreFloat3(&translation, dx::XMVector3Transform(
		dx::XMLoadFloat3(&translation),
		dx::XMMatrixRotationRollPitchYaw(actual.tilt.roll, actual.tilt.pitch, actual.tilt.yaw) *
		dx::XMMatrixScaling(travelSpeed, travelSpeed, travelSpeed)
	));
	actual.pos = {
		actual.pos.x + translation.x,
		actual.pos.y + translation.y,
		actual.pos.z + translation.z
	};
}

void Camera::SetDefaultSpheric() noexcept
{
	sphericRadius = defaultSphericRadius;
	smallRadius = sphericRadius;
	actual = { {0.0f,0.0f,0.0f},  { 0.0f, 0.0f, -sphericRadius } };
	target = { {0.0f,0.0f,0.0f},  { 0.0f, 0.0f, -sphericRadius } };
}

void Camera::SphericMoveUpDown(float delta) noexcept
{

	target.tilt.roll += delta;
	target.tilt.roll = std::min(target.tilt.roll, PI / 2.0f - 0.01f);
	target.tilt.roll = std::max(target.tilt.roll, -PI / 2.0f + 0.01f);
	smallRadius = sphericRadius * cos(target.tilt.roll);
	//pos.y = sphericRadius * sin(roll);
	target.pos = { smallRadius * -sin(target.tilt.pitch), sphericRadius * sin(target.tilt.roll), smallRadius * -cos(target.tilt.pitch) };
}

void Camera::SphericRotate(float delta) noexcept
{
	target.tilt.pitch -= delta;
	if (target.tilt.pitch > PI)
		target.tilt.pitch -= 2 * PI;
	if (target.tilt.pitch < -PI)
		target.tilt.pitch += 2 * PI;
	target.pos = { smallRadius * -sin(target.tilt.pitch), target.pos.y, smallRadius * -cos(target.tilt.pitch) };

}

void Camera::SphericZoom(float delta) noexcept
{
	sphericRadius += delta;
	sphericRadius = std::min(sphericRadius, MAX_SPHERIC_CAMERA_RADIUS);
	sphericRadius = std::max(sphericRadius, MIN_SPHERIC_CAMERA_RADIUS);
	smallRadius = sphericRadius * cos(target.tilt.roll);
	target.pos = { smallRadius * -sin(target.tilt.pitch), sphericRadius * sin(target.tilt.roll), smallRadius * -cos(target.tilt.pitch) };
}

