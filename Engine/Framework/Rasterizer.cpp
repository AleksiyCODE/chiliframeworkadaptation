//coded by Chili
#include "Rasterizer.h"
#include "GraphicsThrowMacros.h"
#include "BindableCodex.h"
#include "ChiliUtil.h"

namespace Bind
{
	Rasterizer::Rasterizer( DXGraphics& gfx,bool twoSided )
		:
		twoSided( twoSided )
	{
		INFOMAN( gfx );

		D3D11_RASTERIZER_DESC rasterDesc = CD3D11_RASTERIZER_DESC( CD3D11_DEFAULT{} );
		rasterDesc.CullMode = twoSided ? D3D11_CULL_NONE : D3D11_CULL_BACK;

		GFX_THROW_INFO( GetDevice( gfx )->CreateRasterizerState( &rasterDesc,&pRasterizer ) );
	}

	void Rasterizer::Bind( DXGraphics& gfx ) noexcept
	{
		GetContext( gfx )->RSSetState( pRasterizer.Get() );
	}
	
	std::shared_ptr<Rasterizer> Rasterizer::Resolve( DXGraphics& gfx,bool twoSided )
	{
		return Codex::Resolve<Rasterizer>( gfx,twoSided );
	}
	std::wstring Rasterizer::GenerateUID( bool twoSided )
	{
		using namespace std::string_literals;
		return ToWide(typeid(Rasterizer).name()) + L"#"s + (twoSided ? L"2s" : L"1s");
	}
	std::wstring Rasterizer::GetUID() const noexcept
	{
		return GenerateUID( twoSided );
	}
}