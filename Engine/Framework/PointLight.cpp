#include "PointLight.h"
#include "../imgui/imgui.h"
#include "FrameCommander.h"

PointLight::PointLight( DXGraphics& gfx, PointLightCBuf& pcbData, __m128* const in_posDat):
	pcbData(pcbData),
	pposcdData(in_posDat),
	cbuf( gfx ),
	poscbuf(gfx, 5)
{}

void PointLight::UpdatePosition(DXGraphics& gfx, DirectX::FXMMATRIX view)
{
	posBuff dataCopy;
	for (size_t i = 0u; i < NUM_OF_LIGHTS; i++)
	{
		const auto pos = DirectX::XMLoadFloat4(&DirectX::XMFLOAT4(pposcdData[i].m128_f32));			//apply camera transform here
		DirectX::XMStoreFloat4(&dataCopy.pos[i], DirectX::XMVector3Transform(pos, view));
	}
	poscbuf.Update(gfx, dataCopy);
}

void PointLight::Bind( DXGraphics& gfx) const noexcept
{
	cbuf.Update( gfx, pcbData);
	cbuf.Bind( gfx );
	poscbuf.Bind(gfx);
}