//coded by Chili
#include "Texture.h"
#include "Surface.h"
#include "GraphicsThrowMacros.h"
#include "BindableCodex.h"
#include "ChiliUtil.h"

namespace Bind
{
	namespace wrl = Microsoft::WRL;

	Texture::Texture( DXGraphics& gfx,const std::wstring& path,UINT slot )
		:
		path( path ),
		slot( slot )
	{
		INFOMAN( gfx );

		// load surface
		const auto s = Surface( path );
		hasAlpha = s.AlphaLoaded();
		width = s.GetWidth();
		height = s.GetHeight();
		// create texture resource
		D3D11_TEXTURE2D_DESC textureDesc = {};
		textureDesc.Width = s.GetWidth();
		textureDesc.Height = s.GetHeight();
		textureDesc.MipLevels = 0;
		textureDesc.ArraySize = 1;
		textureDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.SampleDesc.Quality = 0;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
		wrl::ComPtr<ID3D11Texture2D> pTexture;
		GFX_THROW_INFO( GetDevice( gfx )->CreateTexture2D(
			&textureDesc,nullptr,&pTexture
		) );

		// write image data into top mip level
		GetContext(gfx)->UpdateSubresource(
			pTexture.Get(), 0u, nullptr, s.GetBufferPtrConst(), s.GetWidth() * sizeof(Color), 0u
		);

		// create the resource view on the texture
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.Format = textureDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = -1;
		GFX_THROW_INFO( GetDevice( gfx )->CreateShaderResourceView(
			pTexture.Get(),&srvDesc,&pTextureView
		) );

		// generate the mip chain using the gpu rendering pipeline
		GetContext(gfx)->GenerateMips(pTextureView.Get());
	}

	void Texture::Bind( DXGraphics& gfx ) noexcept
	{
		GetContext( gfx )->PSSetShaderResources( slot,1u,pTextureView.GetAddressOf() );
	}
	std::shared_ptr<Texture> Texture::Resolve( DXGraphics& gfx,const std::wstring& path,UINT slot )
	{
		return Codex::Resolve<Texture>( gfx,path,slot );
	}
	std::wstring Texture::GenerateUID( const std::wstring& path,UINT slot )
	{
		using namespace std::string_literals;
		return ToWide(typeid(Texture).name()) + L"#"s + path + L"#" + std::to_wstring(slot);
	}
	std::wstring Texture::GetUID() const noexcept
	{
		return GenerateUID( path,slot );
	}
	bool Texture::HasAlpha() const noexcept
	{
		return hasAlpha;
	}
	size_t Texture::GetWidth() const noexcept
	{
		return width;
	}
	size_t Texture::GetHeight() const noexcept
	{
		return height;
	}
}
