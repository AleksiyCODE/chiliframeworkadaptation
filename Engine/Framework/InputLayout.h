//coded by Chili
#pragma once
#include "Bindable.h"
#include "Vertex.h"

namespace Bind
{
	class InputLayout : public Bindable
	{
	public:
		InputLayout(DXGraphics& gfx,
			Dvtx::VertexLayout layout,
			ID3DBlob* pVertexShaderBytecode );
		void Bind(DXGraphics& gfx ) noexcept override;
		static std::shared_ptr<InputLayout> Resolve(DXGraphics& gfx,
			const Dvtx::VertexLayout& layout,ID3DBlob* pVertexShaderBytecode );
		static std::wstring GenerateUID( const Dvtx::VertexLayout& layout,ID3DBlob* pVertexShaderBytecode = nullptr );
		std::wstring GetUID() const noexcept override;
	protected:
		Dvtx::VertexLayout layout;
		Microsoft::WRL::ComPtr<ID3D11InputLayout> pInputLayout;
	};
}