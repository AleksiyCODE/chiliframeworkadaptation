//coded by Chili
#pragma once
#include "DXGraphics.h"

class GraphicsResource
{
protected:
	static ID3D11DeviceContext* GetContext(DXGraphics& gfx) noexcept;
	static ID3D11Device* GetDevice(DXGraphics& gfx) noexcept;
	static DxgiInfoManager& GetInfoManager(DXGraphics& gfx);
};