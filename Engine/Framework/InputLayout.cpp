//coded by Chili
#include "InputLayout.h"
#include "GraphicsThrowMacros.h"
#include "BindableCodex.h"
#include "Vertex.h"
#include "ChiliUtil.h"

namespace Bind
{
	InputLayout::InputLayout( DXGraphics& gfx,
		Dvtx::VertexLayout layout_in,
		ID3DBlob* pVertexShaderBytecode )
		:
		layout( std::move( layout_in ) )
	{
		INFOMAN( gfx );

		const auto d3dLayout = layout.GetD3DLayout();

		GFX_THROW_INFO( GetDevice( gfx )->CreateInputLayout(
			d3dLayout.data(),(UINT)d3dLayout.size(),
			pVertexShaderBytecode->GetBufferPointer(),
			pVertexShaderBytecode->GetBufferSize(),
			&pInputLayout
		) );
	}

	void InputLayout::Bind(DXGraphics& gfx ) noexcept
	{
		GetContext( gfx )->IASetInputLayout( pInputLayout.Get() );
	}
	std::shared_ptr<InputLayout> InputLayout::Resolve(DXGraphics& gfx,
			const Dvtx::VertexLayout& layout,ID3DBlob* pVertexShaderBytecode )
	{
		return Codex::Resolve<InputLayout>( gfx,layout,pVertexShaderBytecode );
	}
	std::wstring InputLayout::GenerateUID( const Dvtx::VertexLayout& layout,ID3DBlob* pVertexShaderBytecode )
	{
		return ToWide(typeid(InputLayout).name()) + L"#" + layout.GetCode();
	}
	std::wstring InputLayout::GetUID() const noexcept
	{
		return GenerateUID( layout );
	}
}
