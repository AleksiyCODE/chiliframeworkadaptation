//coded by Chili
#include "Job.h"
#include "Step.h"
#include "Drawable.h"

Job::Job( const Step* pStep,const Drawable* pDrawable )
	:
	pDrawable{ pDrawable },
	pStep{ pStep }
{}

void Job::Execute( DXGraphics& gfx ) const noxnd
{
	if(pStep->Bind(gfx))pDrawable->Bind( gfx );	//check if the draw call is consecutive
	gfx.DrawIndexed( pDrawable->GetIndexCount() );
}