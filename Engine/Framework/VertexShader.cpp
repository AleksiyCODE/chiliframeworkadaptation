//coded by Chili
#include "VertexShader.h"
#include "GraphicsThrowMacros.h"
#include "BindableCodex.h"
#include <typeinfo>
#include"ChiliUtil.h"

namespace Bind
{
	VertexShader::VertexShader( DXGraphics& gfx,const std::wstring& path )
		:
		path( path )
	{
		INFOMAN( gfx );

		GFX_THROW_INFO( D3DReadFileToBlob( std::wstring{path.begin(),path.end()}.c_str(),&pBytecodeBlob ) );
		GFX_THROW_INFO( GetDevice( gfx )->CreateVertexShader(
			pBytecodeBlob->GetBufferPointer(),
			pBytecodeBlob->GetBufferSize(),
			nullptr,
			&pVertexShader
		) );
	}

	void VertexShader::Bind(DXGraphics& gfx ) noexcept
	{
		GetContext( gfx )->VSSetShader( pVertexShader.Get(),nullptr,0u );
	}

	ID3DBlob* VertexShader::GetBytecode() const noexcept
	{
		return pBytecodeBlob.Get();
	}
	std::shared_ptr<VertexShader> VertexShader::Resolve(DXGraphics& gfx,const std::wstring& path )
	{
		return Codex::Resolve<VertexShader>( gfx,path );
	}
	std::wstring VertexShader::GenerateUID( const std::wstring& path )
	{
		return ToWide(typeid(VertexShader).name()) + L"#" + path;
	}
	std::wstring VertexShader::GetUID() const noexcept
	{
		return GenerateUID( path );
	}
}
