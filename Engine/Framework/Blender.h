//coded by Chili
#pragma once
#include "Bindable.h"
#include <array>
#include <optional>

namespace Bind
{
	class Blender : public Bindable
	{
	public:
		Blender( DXGraphics& gfx,bool blending,std::optional<float> factor = {} );
		void Bind( DXGraphics& gfx ) noexcept override;
		void SetFactor( float factor ) noxnd;
		float GetFactor() const noxnd;
		static std::shared_ptr<Blender> Resolve( DXGraphics& gfx,bool blending,std::optional<float> factor = {} );
		//if used without factor will extract alpha from texture
		static std::wstring GenerateUID( bool blending,std::optional<float> factor );
		std::wstring GetUID() const noexcept override;
	protected:
		Microsoft::WRL::ComPtr<ID3D11BlendState> pBlender;
		bool blending;
		std::optional<std::array<float,4>> factors;
	};
}
