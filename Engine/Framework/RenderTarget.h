//coded by Chili
#pragma once
#include "DXGraphics.h"
#include "GraphicsResource.h"

class DepthStencil;

class RenderTarget : public GraphicsResource
{
public:
	RenderTarget(DXGraphics& gfx, size_t width, size_t height);
	void BindAsTexture(DXGraphics& gfx, size_t slot) const noexcept;
	void BindAsTarget(DXGraphics& gfx) const noexcept;
	void BindAsTarget(DXGraphics& gfx, const DepthStencil& depthStencil) const noexcept;
	void Clear(DXGraphics& gfx);
private:
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> pTextureView;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView> pTargetView;
	static constexpr FLOAT Black[4] = { 0.0f,0.0f,0.0f,0.0f };
};