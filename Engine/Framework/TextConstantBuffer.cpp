#include "TextConstantBuffer.h"
#include "ChiliMath.h"
#include <math.h>
#include "Vec3.h"
namespace Bind
{
	TextConstantBuffer::TextConstantBuffer(DXGraphics& gfx, UINT slot)
	{
		if (!pVcbuf)
		{
			pVcbuf = std::make_unique<VertexConstantBuffer<Transforms2D>>(gfx, slot);
		}
		if (!pPcbuf)
		{
			pPcbuf = std::make_unique<PixelConstantBuffer<TextColor>>(gfx, slot);
		}
	}

	void TextConstantBuffer::Bind(DXGraphics& gfx) noexcept
	{
		UpdateBindImpl(gfx, GetTransforms(gfx));
	}

	void TextConstantBuffer::InitializeParentReference(const Drawable& parent) noexcept
	{
		pParent = &parent;
	}

	void TextConstantBuffer::UpdateBindImpl(DXGraphics& gfx, const TextData& tf) noexcept
	{
		assert(pParent != nullptr);
		pVcbuf->Update(gfx, tf.trans);
		pVcbuf->Bind(gfx);
		pPcbuf->Update(gfx, tf.col);
		pPcbuf->Bind(gfx);
	}

	TextConstantBuffer::TextData TextConstantBuffer::GetTransforms(DXGraphics& gfx) noexcept
	{
		assert(pParent != nullptr);
		const auto encoded = pParent->GetTransformXM();		
		DirectX::XMFLOAT4X4 stored;
		DirectX::XMStoreFloat4x4(&stored, encoded);
		TextData td;
		td.trans.rotation = stored._13;
		td.trans.translation = { stored._11,stored._12 };
		td.trans.scale = stored._14;
		td.col.color = { stored._21,stored._22, stored._23 };
		return std::move(td);
	}
	std::unique_ptr<VertexConstantBuffer<TextConstantBuffer::Transforms2D>> TextConstantBuffer::pVcbuf;
	std::unique_ptr<PixelConstantBuffer<TextConstantBuffer::TextColor>> 	TextConstantBuffer::pPcbuf;
}