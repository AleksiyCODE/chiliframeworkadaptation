//coded by Chili
#pragma once
#include "Bindable.h"

namespace Bind
{
	class PixelShader : public Bindable
	{
	public:
		PixelShader(DXGraphics& gfx,const std::wstring& path );
		void Bind(DXGraphics& gfx ) noexcept override;
		static std::shared_ptr<PixelShader> Resolve(DXGraphics& gfx,const std::wstring& path );
		static std::wstring GenerateUID( const std::wstring& path );
		std::wstring GetUID() const noexcept override;
	protected:
		std::wstring path;
		Microsoft::WRL::ComPtr<ID3D11PixelShader> pPixelShader;
	};
}