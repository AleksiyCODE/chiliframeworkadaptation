//partly coded by Chili
#pragma once
#include "DXGraphics.h"
#include "Vec3.h"
#include <math.h>

#define MAX_SPHERIC_CAMERA_RADIUS 25.0f
#define MIN_SPHERIC_CAMERA_RADIUS 2.0f

struct CamTilt
{
	float roll = 0.0f;
	float pitch = 0.0f;
	float yaw = 0.0f;
	CamTilt& operator+=(const CamTilt& rhs)
	{
		roll += rhs.roll;
		pitch += rhs.pitch;
		yaw += rhs.yaw;
		return *this;
	} 
	friend CamTilt operator+(CamTilt lhs, const CamTilt& rhs)
	{
		lhs += rhs; 
		return lhs; 
	}
	CamTilt operator*(float f)
	{
		CamTilt ct = *this;
		ct.roll *= f;
		ct.pitch *= f;
		ct.yaw *= f;
		return ct;
	}
	CamTilt& operator*=(float f)
	{
		roll *=  f;
		pitch *= f;
		yaw *=   f;
		return *this;
	}
};
struct CamMove
{
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
	CamMove& operator+=(const CamMove& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		return *this;
	}
	friend CamMove operator+(CamMove lhs, const CamMove& rhs)
	{
		lhs += rhs;
		return lhs;
	}	
	CamMove operator*(float f)
	{
		CamMove cm = *this;
		cm.x *= f;
		cm.y *= f;
		cm.z *= f;
		return cm;
	}	
	CamMove& operator*=(float f)
	{
		x *= f;
		y *= f;
		z *= f;
		return *this;
	}
};
struct CamTrans
{
	CamTilt tilt;
	CamMove pos;
	CamTrans& operator+=(const CamTrans& rhs)
	{
		tilt += rhs.tilt;
		pos += rhs.pos;
		return *this;
	}
	friend CamTrans operator+(CamTrans lhs, const CamTrans& rhs)
	{
		lhs += rhs;
		return lhs;
	}
	CamTrans operator*(float f)
	{
		CamTrans ct = *this;
		ct.tilt *= f;
		ct.pos *= f;
		return ct;
	}	
};
class Camera
{
public:
	Camera() noexcept;
	DirectX::XMMATRIX GetMatrix() const noexcept;		//supposed to be callsed once/frame for better interpolation
	DirectX::XMFLOAT3 GetPosition() const noexcept;
	void SpawnControlWindow() noexcept;
	void Update() noexcept;
	void Reset() noexcept;
	void SetFullTransform(CamTrans in_trans);
	void SetBaseDirection(Vec3 baseDir)
	{
		forwardVector = baseDir;
	}
	Vec3 GetBaseDirection(Vec3 baseDir)
	{
		return forwardVector;
	}
	void SetUpDirection(Vec3 up)
	{
		normalVector = up;
	}
	Vec3 GetUpDirection()
	{
		return normalVector;
	}
	void Rotate( float dx,float dy ) noexcept;
	void SphereSlide(float dx, float dy) noexcept;
	void Translate( DirectX::XMFLOAT3 translation ) noexcept;

	//for spheric camera
	void SetDefaultSpheric() noexcept;
	void SphericMoveUpDown(float delta) noexcept;
	void SphericRotate(float delta) noexcept;
	void SphericZoom(float delta) noexcept;
private:
	CamTrans actual{ {0.0f,3.0f * PI / 4.0f,0.0f}, { -6.0f,-1.0f,6.0f } };
	CamTrans target{ {0.0f,3.0f * PI / 4.0f,0.0f}, { -6.0f,-1.0f,6.0f } };
	// for spheric camera
	const float defaultSphericRadius = 7.0f;

	float sphericRadius = defaultSphericRadius;
	float smallRadius = defaultSphericRadius;

	float actualSphericRadius = sphericRadius;
	float actualSmallRadius = smallRadius;

	static constexpr float travelSpeed = 7.0f;
	static constexpr float rotationSpeed = 0.004f;
	static constexpr float sphericRotationSpeed = 0.004f;
	static constexpr float dragCoeff = 0.8f;

	Vec3 forwardVector{0.0f,0.0f,1.0f};
	Vec3 normalVector{0.0f,1.0f,0.0f};
	const Vec3 defaultForward{ 0.0f,0.0f,1.0f };
	const Vec3 defaultUp{ 0.0f,1.0f,0.0f };
};