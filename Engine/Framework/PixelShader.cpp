//coded by Chili
#include "PixelShader.h"
#include "GraphicsThrowMacros.h"
#include "BindableCodex.h"
#include "ChiliUtil.h"

namespace Bind
{
	PixelShader::PixelShader(DXGraphics& gfx,const std::wstring& path )
		:
		path( path )
	{
		INFOMAN( gfx );

		Microsoft::WRL::ComPtr<ID3DBlob> pBlob;
		GFX_THROW_INFO( D3DReadFileToBlob( std::wstring{path.begin(),path.end()}.c_str(),&pBlob ) );
		GFX_THROW_INFO( GetDevice( gfx )->CreatePixelShader( pBlob->GetBufferPointer(),pBlob->GetBufferSize(),nullptr,&pPixelShader ) );
	}

	void PixelShader::Bind(DXGraphics& gfx ) noexcept
	{
		GetContext( gfx )->PSSetShader( pPixelShader.Get(),nullptr,0u );
	}
	std::shared_ptr<PixelShader> PixelShader::Resolve(DXGraphics& gfx,const std::wstring& path )
	{
		return Codex::Resolve<PixelShader>( gfx,path );
	}
	std::wstring PixelShader::GenerateUID( const std::wstring& path )
	{
		return ToWide(typeid(PixelShader).name()) + L"#" + path;
	}
	std::wstring PixelShader::GetUID() const noexcept
	{
		return GenerateUID( path );
	}
}
