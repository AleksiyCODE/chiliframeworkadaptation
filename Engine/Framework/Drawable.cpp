//partly coded by Chili
#include "Drawable.h"
#include "GraphicsThrowMacros.h"
#include "BindableCommon.h"
#include "BindableCodex.h"

using namespace Bind;

void Drawable::Submit(FrameCommander& frame, Techniques tec) const noexcept
{
	assert(static_cast<size_t>(tec) <= techniques.size() && "techniques provided for this drawable do not mach the requested one");
	techniques[static_cast<size_t>(tec)].Submit(frame, *this);
}

void Drawable::RebindParent()
{
	for (auto& t : techniques) t.InitializeParentReferences(*this);
}

void Drawable::AddTechnique(Technique tech_in) noexcept
{
	tech_in.InitializeParentReferences(*this);
	techniques.push_back(std::move(tech_in));
}

void Drawable::Bind(DXGraphics& gfx) const noexcept
{
	pIndices->Bind(gfx);
	pVertices->Bind(gfx);
}

UINT Drawable::GetIndexCount() const noxnd
{
	return pIndices->GetCount();
}

std::vector<Technique>& Drawable::GetTechniquesReference()
{
	return techniques;
}

PositionData& PositionData::operator=(PositionData pd)
{
	pos.x = pd.pos.x;
	pos.y = pd.pos.y;
	pos.z = pd.pos.z;
	rot.x = pd.rot.x;
	rot.y = pd.rot.y;
	rot.z = pd.rot.z;
	return *this;
}

PositionData& PositionData::operator=(PositionData&& pd)
{
	pos = (std::move(pd.pos));
	rot = (std::move(pd.rot));
	return *this;
}
