//mostly coded by Chili
#pragma once
#include "ChiliWin.h"
#include <vector>
#include "ChiliException.h"
#include <d3d11.h>
#include <string>
#include "DxgiInfoManager.h"
#include "MyWRL.h"
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <memory>
#include <random>
#include "ConditionalNoexcept.h"

namespace Bind
{
	class Bindable;
}

class DXGraphics
{
	friend class GraphicsResource;
public:
	class Exception : public ChiliException
	{
		using ChiliException::ChiliException;
	};
	class HrException : public Exception
	{
	public:
		HrException(int line, const wchar_t* file, HRESULT hr, std::vector<std::wstring> infoMsgs = {}) noexcept;
		const wchar_t* MyWhat() const noexcept override;
		const wchar_t* GetType() const noexcept override;
		HRESULT GetErrorCode() const noexcept;
		std::wstring GetErrorString() const noexcept;
		std::wstring GetErrorDescription() const noexcept;
		std::wstring GetErrorInfo() const noexcept;
	private:
		HRESULT hr;
		std::wstring info;
	};
	class InfoException : public Exception
	{
	public:
		InfoException(int line, const wchar_t* file, std::vector<std::wstring> infoMsgs) noexcept;
		const wchar_t* MyWhat() const noexcept override;
		const wchar_t* GetType() const noexcept override;
		std::wstring GetErrorInfo() const noexcept;
	private:
		std::wstring info;
	};
	class DeviceRemovedException : public HrException
	{
		using HrException::HrException;
	public:
		const wchar_t* GetType() const noexcept override;
	private:
		std::wstring reason;
	};
	void SetProjection(DirectX::FXMMATRIX proj) noexcept;
	DirectX::XMMATRIX GetProjection() const noexcept;
	void SetCamera(DirectX::FXMMATRIX cam) noexcept;
	DirectX::XMMATRIX GetCamera() const noexcept;
	void SetCamPos(DirectX::XMFLOAT3 cam) noexcept;
	DirectX::XMFLOAT3 GetCamPos() const noexcept;
	void EnableImgui() noexcept;
	void DisableImgui() noexcept;
	void DrawIndexed(UINT count) noxnd;
	bool IsImguiEnabled() const noexcept;
	DXGraphics(HWND hWnd, int width, int height);
	DXGraphics(const DXGraphics&) = delete;
	DXGraphics& operator=(const DXGraphics&) = delete;
	~DXGraphics();
	void EndFrame();
	void BeginFrame(float red, float green, float blue) noexcept;
	void BindSwapBuffer() noexcept;
	void BindSwapBuffer(const class DepthStencil& ds) noexcept;
	size_t GetScreenWidth() { return screenWidth; }
	size_t GetScreenHeight() { return screenHeight; }
private:
	size_t screenWidth;
	size_t screenHeight;
	DirectX::XMMATRIX projection;
	DirectX::XMMATRIX camera;
	DirectX::XMFLOAT3 camPos;
	bool imguiEnabled = true;
#ifndef NDEBUG
	DxgiInfoManager infoManager;
#endif
	Microsoft::WRL::ComPtr<ID3D11Device> pDevice;
	Microsoft::WRL::ComPtr<IDXGISwapChain> pSwap ;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext> pContext ;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView> pTarget;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView> pDSV;
};