//partly coded by Chili
#pragma once
#include "ConstantBuffers.h"
#include "Drawable.h"
#include <DirectXMath.h>

namespace Bind
{
	class TransformCbuf : public Bindable
	{
	public:
		enum class DataType
		{
			Standart,
			CamFacing,				//only works with posData.rot = { 0.0f,  3 * PI / 2, ... }; use ... to change roll (PI/2.0f - no roll)
			NotSpecified		
		};
	protected:
		struct Transforms
		{
			DirectX::XMMATRIX modelView;
			DirectX::XMMATRIX modelViewProj;
		};
	public:
		TransformCbuf(DXGraphics& gfx,DataType type = DataType::Standart, UINT slot = 0u);
		void Bind(DXGraphics& gfx) noexcept override;
		void InitializeParentReference(const Drawable& parent) noexcept override;
		std::wstring GetUID() const noexcept override;
	protected:
		void UpdateBindImpl(DXGraphics& gfx, const Transforms& tf) noexcept;
		Transforms GetTransforms(DXGraphics& gfx) noexcept;
	private:
		static std::unique_ptr<VertexConstantBuffer<Transforms>> pVcbuf;
		const Drawable* pParent = nullptr;
		const DataType datTyp;
	};
}