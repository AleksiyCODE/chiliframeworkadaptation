//coded by Chili
#pragma once
#include "Bindable.h"

namespace Bind
{
	class VertexShader : public Bindable
	{
	public:
		VertexShader(DXGraphics& gfx,const std::wstring& path );
		void Bind(DXGraphics& gfx ) noexcept override;
		ID3DBlob* GetBytecode() const noexcept;
		static std::shared_ptr<VertexShader> Resolve(DXGraphics& gfx,const std::wstring& path );
		static std::wstring GenerateUID( const std::wstring& path );
		std::wstring GetUID() const noexcept override;
	protected:
		std::wstring path;
		Microsoft::WRL::ComPtr<ID3DBlob> pBytecodeBlob;
		Microsoft::WRL::ComPtr<ID3D11VertexShader> pVertexShader;
	};
}