#include "FrameCommander.h"

void FrameCommander::Execute(DXGraphics& gfx, float dt) noxnd			
{
	ds.Clear(gfx);
	using namespace Bind;
	Rasterizer::Resolve(gfx, true)->Bind(gfx);		//backface culling is not needed, because there are literary no backfaces
	//Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST)->Bind(gfx);		//all models must use it
	Stencil::Resolve(gfx, Stencil::Mode::HUD)->Bind(gfx);
	Sampler::Resolve(gfx)->Bind(gfx);
	gfx.BindSwapBuffer(ds);
	if (blurMod == 0.0f)
	{								//draw without blur
		passes[0].Execute(gfx);					//most models
	//	Stencil::Resolve(gfx, Stencil::Mode::HUD)->Bind(gfx);	//turns out it looks better if pass1 models are drawn without depth stencilling
		passes[1].Execute(gfx);
	}
	else
	{
		UINT kernelSize = 10;
		bp.SetKernelGauss(gfx, (size_t)kernelSize, 3);
		rt1.BindAsTarget(gfx, ds);
		passes[0].Execute(gfx);					//most models

		// fullscreen blur h-pass
		rt2.BindAsTarget(gfx);
		rt1.BindAsTexture(gfx, 0);
		pVbFull->Bind(gfx);
		pIbFull->Bind(gfx);
		pVsFull->Bind(gfx);
		pLayoutFull->Bind(gfx);
		bp.Bind(gfx);
		Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST)->Bind(gfx);		//all models must use it
		bp.SetHorizontal(gfx);
		gfx.DrawIndexed(pIbFull->GetCount());
		// fullscreen blur v-pass
		gfx.BindSwapBuffer(ds);
		passes[1].Execute(gfx);		//background




		Blender::Resolve(gfx, true)->Bind(gfx);		//turn on blending not to override background
		
		
		
		
		pVbFull->Bind(gfx);
		pIbFull->Bind(gfx);
		pVsFull->Bind(gfx);
		pLayoutFull->Bind(gfx);
		//Rasterizer::Resolve(gfx, false)->Bind(gfx);		//backface culling is not needed, because there are literary no backfaces
		//Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST)->Bind(gfx);		//all models must use it
		//Stencil::Resolve(gfx, Stencil::Mode::HUD)->Bind(gfx);
		//Sampler::Resolve(gfx)->Bind(gfx);
		bp.Bind(gfx);

		rt2.BindAsTexture(gfx, 0u);
		bp.SetVertical(gfx);
		gfx.DrawIndexed(pIbFull->GetCount());
		Blender::Resolve(gfx, false)->Bind(gfx);//turn off blending, because we don't need it anymore
		Stencil::Resolve(gfx, Stencil::Mode::Default)->Bind(gfx);	
		rt1.Clear(gfx);
		rt2.Clear(gfx);
		passes[0].Execute(gfx);					//most models

	}
	//Stencil::Resolve(gfx, Stencil::Mode::HUD)->Bind(gfx);			//it is bound earlier
	passes[3].Execute(gfx);									//HUD
	passes[4].Execute(gfx);									//text
}									//if the passes need to be rearranged in some way
									//than all darwables that depend on them should be ajusted accordingly

FrameCommander::FrameCommander(DXGraphics& gfx) :
	gfx(gfx),
	ds(gfx, gfx.GetScreenWidth(), gfx.GetScreenHeight()),
	rt1(gfx, gfx.GetScreenWidth(), gfx.GetScreenHeight()),
	rt2(gfx, gfx.GetScreenWidth(), gfx.GetScreenHeight()),
	bp(gfx)
{
	namespace dx = DirectX;
	// setup fullscreen geometry
	Dvtx::VertexLayout lay;
	lay.Append(Dvtx::VertexLayout::Position2D);
	Dvtx::VertexBuffer bufFull{ lay };
	bufFull.EmplaceBack(dx::XMFLOAT2{ -1,1 });
	bufFull.EmplaceBack(dx::XMFLOAT2{ 1,1 });
	bufFull.EmplaceBack(dx::XMFLOAT2{ -1,-1 });
	bufFull.EmplaceBack(dx::XMFLOAT2{ 1,-1 });
	pVbFull = Bind::VertexBuffer::Resolve(gfx, L"$Full", std::move(bufFull));
	std::vector<unsigned short> indices = { 0,1,2,1,3,2 };
	pIbFull = Bind::IndexBuffer::Resolve(gfx, L"$Full", std::move(indices));
	// setup fullscreen shaders
	pVsFull = Bind::VertexShader::Resolve(gfx, L"Shaders\\FullscreenVS.cso");
	pLayoutFull = Bind::InputLayout::Resolve(gfx, lay, pVsFull->GetBytecode());
}

void FrameCommander::Reset() noexcept
{
	for (auto& p : passes)
	{
		p.Reset();
	}
}

void FrameCommander::SetBlurMod(float blur)
{
	blurMod = blur;
}

float FrameCommander::blurMod = 0.0f;