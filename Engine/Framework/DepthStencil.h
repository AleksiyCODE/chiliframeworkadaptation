#pragma once
#include "DXGraphics.h"
#include "GraphicsResource.h"

class DepthStencil : public GraphicsResource
{
	friend class RenderTarget;
	friend class DXGraphics;
public:
	DepthStencil(DXGraphics& gfx, size_t width, size_t height);
	void BindAsDepthStencil(DXGraphics& gfx) const noexcept;
	void Clear(DXGraphics& gfx) const noexcept;
private:
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView> pDepthStencilView;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState> pStencil;
};
