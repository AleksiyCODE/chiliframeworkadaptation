#pragma once
#include "ConstantBuffers.h"
#include "Drawable.h"
#include <DirectXMath.h>

namespace Bind
{			//the owner of this bindable must have its GetTransformXM() function overriden
			//in the same way as in Letter.h
	class TextConstantBuffer : public Bindable
	{
	public:
		struct Transforms2D
		{
			DirectX::XMFLOAT2 translation;
			float rotation;
			float scale;
		};
		struct TextColor
		{
			DirectX::XMFLOAT3 color;
			float padding;
		};
		struct TextData
		{
			Transforms2D	trans;
			TextColor col;
		};
	public:
		TextConstantBuffer(DXGraphics& gfx, UINT slot = 0u);
		void Bind(DXGraphics& gfx) noexcept override;
		void InitializeParentReference(const Drawable& parent) noexcept override;
	protected:
		void UpdateBindImpl(DXGraphics& gfx, const TextData& tf) noexcept;
		TextData GetTransforms(DXGraphics& gfx) noexcept;
	private:
		static std::unique_ptr<VertexConstantBuffer<Transforms2D>> pVcbuf;
		static std::unique_ptr<PixelConstantBuffer<TextColor>> pPcbuf;
		const Drawable* pParent = nullptr;
	};
}