//coded by Chili
#pragma once
#include "Bindable.h"

namespace Bind
{
	class Sampler : public Bindable
	{
	public:
		Sampler(DXGraphics& gfx );
		void Bind(DXGraphics& gfx ) noexcept override;
		static std::shared_ptr<Sampler> Resolve(DXGraphics& gfx );
		static std::wstring GenerateUID();
		std::wstring GetUID() const noexcept override;
	protected:
		Microsoft::WRL::ComPtr<ID3D11SamplerState> pSampler;
	};
}
