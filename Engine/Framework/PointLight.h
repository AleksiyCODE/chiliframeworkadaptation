#pragma once
#include "../Shaders/NumOfLights.hlsl"						
#include "DXGraphics.h"
#include "ConstantBuffers.h"
#include "Vec4.h"

struct alignas(16) PointLightCBuf
{
	__m128 ambient	   [NUM_OF_LIGHTS];			
	__m128 diffuseColor[NUM_OF_LIGHTS];
	Vec4 intensityParam[NUM_OF_LIGHTS];         //first float - intencity, second - constanf attenuation, third - linear, fourth - quadratic
};

class PointLight   //this class sends data about point lights of the scene to GPU
{
public:
	PointLight( DXGraphics& gfx, PointLightCBuf& pcbData, __m128* const pposcdData);
	void UpdatePosition(DXGraphics& gfx, DirectX::FXMMATRIX view);			
	void Bind( DXGraphics& gfx) const noexcept;
private:
	PointLightCBuf& pcbData;			
	const __m128* const pposcdData;
	mutable Bind::PixelConstantBuffer<PointLightCBuf> cbuf;
	struct posBuff
	{
		DirectX::XMFLOAT4 pos[NUM_OF_LIGHTS];
	};
	mutable Bind::VertexConstantBuffer<posBuff> poscbuf;
};