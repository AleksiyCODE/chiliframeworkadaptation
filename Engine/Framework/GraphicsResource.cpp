//coded by Chili
#include "GraphicsResource.h"

ID3D11DeviceContext* GraphicsResource::GetContext(DXGraphics& gfx) noexcept
{
	return gfx.pContext.Get();
}

ID3D11Device* GraphicsResource::GetDevice(DXGraphics& gfx) noexcept
{
	return gfx.pDevice.Get();
}

DxgiInfoManager& GraphicsResource::GetInfoManager(DXGraphics& gfx)
{
#ifndef NDEBUG
	return gfx.infoManager;
#else
	throw std::logic_error("YouFuckedUp! (tried to access gfx.infoManager in Release config)");
#endif
}
