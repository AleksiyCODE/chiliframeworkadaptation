//partly coded by Chili
#pragma once
#include "Bindable.h"
#include "ChiliUtil.h"
namespace Bind
{
	class Stencil : public Bindable
	{
	public:
		enum class Mode
		{
			Default,
			Write,
			Mask,
			HUD,					//disables depth
			ReadOnly
		};
		Stencil( DXGraphics& gfx,Mode mode ):
			mode( mode )
		{
			D3D11_DEPTH_STENCIL_DESC dsDesc = CD3D11_DEPTH_STENCIL_DESC{ CD3D11_DEFAULT{} };
			if (mode == Mode::Write)
			{
				dsDesc.DepthEnable = FALSE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
				dsDesc.StencilEnable = TRUE;
				dsDesc.StencilWriteMask = 0xFF;
				dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
				dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_REPLACE;
			}
			else if (mode == Mode::Mask)
			{
				dsDesc.DepthEnable = FALSE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
				dsDesc.StencilEnable = TRUE;
				dsDesc.StencilReadMask = 0xFF;
				dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_NOT_EQUAL;
				dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			}
			else if (mode == Mode::HUD)
			{
				dsDesc.DepthEnable = FALSE;		
			}
			else if (mode == Mode::ReadOnly)
			{
				dsDesc.DepthEnable = TRUE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
			}
			GetDevice( gfx )->CreateDepthStencilState( &dsDesc,&pStencil );
		}
		void Bind( DXGraphics& gfx ) noexcept override
		{
			GetContext( gfx )->OMSetDepthStencilState( pStencil.Get(),0xFF );
		}
		static std::shared_ptr<Stencil> Resolve( DXGraphics& gfx,Mode mode )
		{
			return Codex::Resolve<Stencil>( gfx,mode );
		}
		static std::wstring GenerateUID( Mode mode )
		{
			using namespace std::string_literals;
			const auto modeName = [mode]() {
				switch( mode ) {
				case Mode::Default:
					return "off"s;
				case Mode::Write:
					return "write"s;
				case Mode::Mask:
					return "mask"s;	
				case Mode::HUD:
						return "HUD"s;
				case Mode::ReadOnly:
						return "ReadOnly"s;
				}
				assert(false && "Bad stencil");
				return "ERROR"s;
			};
			return ToWide(typeid(Stencil).name() + "#"s + modeName());
		}
		std::wstring GetUID() const noexcept override
		{
			return GenerateUID( mode );
		}
	private:
		Mode mode;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilState> pStencil;
	};
}
