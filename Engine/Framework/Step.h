//mostly coded by Chili
#pragma once
#include <vector>
#include <memory>
#include "Bindable.h"
#include "DXGraphics.h"

class Step
{
public:
	Step(size_t targetPass_in);
	Step(const Step&) = default;
	void AddBindable(std::shared_ptr<Bind::Bindable> bind_in) noexcept;
	void PopBindable() noexcept;
	void DeleteBindableByUID(std::wstring ID);
	void Submit( class FrameCommander& frame,const class Drawable& drawable ) const;
	bool Bind(DXGraphics& gfx) const;
	void SetTargetPass(size_t newPass);				//for cheap altering steps
	std::vector<std::shared_ptr<Bind::Bindable>>& GetBindables()
	{
		return bindables;
	}
	void InitializeParentReferences( const class Drawable& parent ) noexcept;
private:
	size_t targetPass;
	std::vector<std::shared_ptr<Bind::Bindable>> bindables;
};