//coded by Chili
#pragma once
#include "Bindable.h"

namespace Bind
{
	class Topology : public Bindable
	{
	public:
		Topology(DXGraphics& gfx,D3D11_PRIMITIVE_TOPOLOGY type );
		void Bind(DXGraphics& gfx ) noexcept override;
		static std::shared_ptr<Topology> Resolve(DXGraphics& gfx,D3D11_PRIMITIVE_TOPOLOGY type );
		static std::wstring GenerateUID( D3D11_PRIMITIVE_TOPOLOGY type );
		std::wstring GetUID() const noexcept override;
	protected:
		D3D11_PRIMITIVE_TOPOLOGY type;
	};
}