//partly coded by Chili
#include "TransformCbuf.h"
#include "ChiliMath.h"
#include <math.h>
#include "Vec3.h"
namespace Bind
{
	TransformCbuf::TransformCbuf(DXGraphics& gfx, DataType type , UINT slot) :
		datTyp(type)
	{
		if (!pVcbuf)				//checking because it is a static variable
		{
			pVcbuf = std::make_unique<VertexConstantBuffer<Transforms>>(gfx, slot);
		}
	}

	void TransformCbuf::Bind(DXGraphics& gfx) noexcept
	{
		UpdateBindImpl(gfx, GetTransforms(gfx));
	}

	void TransformCbuf::InitializeParentReference(const Drawable& parent) noexcept
	{
		pParent = &parent;
	}

	std::wstring TransformCbuf::GetUID() const noexcept
	{
		return L"TransformCbuf" + std::to_wstring(static_cast<size_t>(datTyp));
	}

	void TransformCbuf::UpdateBindImpl(DXGraphics& gfx, const Transforms& tf) noexcept
	{
		assert(pParent != nullptr);
		pVcbuf->Update(gfx, tf);
		pVcbuf->Bind(gfx);
	}

	TransformCbuf::Transforms TransformCbuf::GetTransforms(DXGraphics& gfx) noexcept
	{
		assert(pParent != nullptr);
		switch (datTyp)
		{
		case Bind::TransformCbuf::DataType::CamFacing:
		{
			namespace dx = DirectX;

			const auto modelTransform = pParent->GetTransformXM();	//world
			const auto camera = gfx.GetCamera();					//view
			const auto camPos = gfx.GetCamPos();

			dx::XMFLOAT4X4 modelTransStored;
			dx::XMStoreFloat4x4(&modelTransStored, modelTransform);
			dx::XMFLOAT4X4 viewStored;
			dx::XMStoreFloat4x4(&viewStored, camera);

			Vec3 worldPos{ modelTransStored._41, modelTransStored._42, modelTransStored._43 };	//Vec3 is used because it has extra operations defined 
			Vec3 camPosVec{ camPos.x, camPos.y, camPos.z };
			Vec3 diff = camPosVec - worldPos;

			float cosDeltaAngle = (diff / diff.Len()) * Vec3{ 0.0f, 0.0f, -1.0f };	//no explanation here...
																					//just know that works ONLY with posData.rot = { 0.0f,  3 * PI / 2, ... };
																					//use ... to change roll (PI/2.0f - no roll)
			dx::XMMATRIX newWorldTrans;
			if (diff.x > 0.0f)
			{
				newWorldTrans = dx::XMMatrixRotationX(-acos(cosDeltaAngle)) * modelTransform;
			}
			else
			{
				newWorldTrans = dx::XMMatrixRotationX(acos(cosDeltaAngle)) * modelTransform;
			}
			dx::XMMATRIX newWorldView = dx::XMMatrixMultiply(newWorldTrans, camera);
			return {
				dx::XMMatrixTranspose(newWorldView),
				dx::XMMatrixTranspose(newWorldView * gfx.GetProjection())
			};
			break;
		}
		case Bind::TransformCbuf::DataType::NotSpecified:
			assert(false && "please, specify data type fot transform constant buffer");
		//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
		case Bind::TransformCbuf::DataType::Standart:
		{
			const auto modelView = pParent->GetTransformXM() * gfx.GetCamera();
			return {
				DirectX::XMMatrixTranspose(modelView),
				DirectX::XMMatrixTranspose(modelView * gfx.GetProjection())
			};
		}
		break;
		default:
			assert(false && "programmer missed a case in TransformCbuf::GetTransforms(DXGraphics& gfx)");
			{
				const auto modelView = pParent->GetTransformXM() * gfx.GetCamera();
				return {
					DirectX::XMMatrixTranspose(modelView),
					DirectX::XMMatrixTranspose(modelView * gfx.GetProjection())
				};
			}
			break;
		}		
	}
	std::unique_ptr<VertexConstantBuffer<TransformCbuf::Transforms>> TransformCbuf::pVcbuf;
}