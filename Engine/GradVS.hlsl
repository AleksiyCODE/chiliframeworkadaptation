#include "Shaders/Transform.hlsl"
struct VSOut
{
    float hight : Hight; //0 - closest to camera; 1 - farthest
    float4 SVpos : SV_Position;
};

VSOut main(float4 pos : POSITION)
{
    VSOut vso;
    vso.SVpos = mul((pos), modelViewProj);
    vso.hight = pos.y;
    return vso;
}