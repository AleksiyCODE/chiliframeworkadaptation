#include "Shaders/Transform.hlsl"
#include "Shaders/Scale.hlsl"
struct VSOut
{
    float dist : CentDist;
    float4 SVpos : SV_Position;
};

VSOut main(float3 pos : Position)
{
    VSOut vso;
#ifdef SCALE
    pos*=scale;
#endif    
    vso.dist = sqrt(pos.x * pos.x + pos.y * pos.y + pos.z * pos.z);
    vso.SVpos = mul(float4(pos, 1.0f), modelViewProj);
    return vso;
}