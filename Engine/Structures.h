#pragma once
#include <ostream>

struct handParam {
	float asc;
	float rot;
	float pit;
	float r;
	float vasc;
	float vrot;
	float vpit;
	handParam() = default;
	handParam(float a, float r, float p, float R, float va, float vr, float vp) :
		asc(a),
		rot(r),
		pit(p),
		r(R),
		vasc(va),
		vrot(vr),
		vpit(vp) {};
};
std::ostream& operator<<(std::ostream& os, const handParam& obj);

enum class ComputingMode
{
	Classic,
	Uniform,
	Parallel,
	Meredian,
	Wavy,
	Last
};

enum class Techniques									
{
	White,
	Lighted,
	Rainbow,
	CrawlingRainbow,
	Custom,
	Last
};