cbuffer TransformCBuf : register(b3)
{
    float fillness;         //playerFill/enemFill
    float3 padding;
};
Texture2D playerInit;
Texture2D enemyInit : register(t3);
SamplerState splr;
float4 main(float2 tc : Texcoord, float4 pos : SV_Position) : SV_Target
{
    float wholeFill = fillness + 1.0f;  //whole fillness
    float relFill = fillness / wholeFill;   //part of bar that player takes up
    float drelFil = 0.5f - relFill;
    relFill += (2.0f * drelFil);
    float4 dtex;
    if (relFill > tc.y)
    {
        dtex = enemyInit.Sample(splr, tc);
    }
    else
    {
        dtex = playerInit.Sample(splr, tc);
    }
    return float4(dtex.rgba);
}



