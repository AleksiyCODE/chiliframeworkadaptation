#pragma once
#include "Draws.h"
#include "Framework\FrameCommander.h"
#include "Framework\DXGraphics.h"
#include "Framework\VertexBuffer.h"

class BurstManager
{
public:
	enum class Type
	{
		Particle,
		Spagetty
	};
	BurstManager(DXGraphics& gfx);
	void UpdateBursts(float dt, DXGraphics& gfx);
	void SubmitBursts(FrameCommander& fc);
	void InitiateBurstFromTrail(const Bind::VertexBuffer& vb, BurstManager::Type type);
	struct BurstData
	{
		BurstData(ParticleBurst&& b, BurstManager::Type type);
		ParticleBurst burst;
		float lifeTime = 1.0f;
		BurstManager::Type type;
		Vec3(*dispFun) (Vec3);
	};
private:
	std::vector<BurstData> bursts;
	static constexpr float decaySpeed = 0.14f;
	DXGraphics& gfx;
};

namespace ParticleDespersionFunctions
{
	constexpr float moveIntencity = 6.8f;
	Vec3 DispersionFun(Vec3 pos);
	Vec3 AwayFun(Vec3 pos);
	Vec3 ImplotionFun(Vec3 pos);
	Vec3 TwisterFun(Vec3 pos);
	Vec3 VortexFun(Vec3 pos);
	Vec3 ScatterFun(Vec3 pos);
	Vec3 StretchFun(Vec3 pos);
}