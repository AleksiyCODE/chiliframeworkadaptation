#pragma once
#include "Framework\Drawable.h"
#include"Framework\Vec4.h"
#include "Framework\ConstantBuffers.h"
#include <optional>

class Line : public Drawable
{
public:
	struct Brightness
	{
		float brightness;
		Vec3 padding;
	};
	struct CountBuf
	{
		int vertexCount;
		Vec3 padding;
	};
	struct Color
	{
		Vec4 col;
	};
	//struct RainGrad
	//{
	//	float grad;
	//	Vec3 padding;
	//};
	Line(DirectX::XMFLOAT3 beg, DirectX::XMFLOAT3 end, DXGraphics& gfx);
	Line(DirectX::XMFLOAT3 beg, DXGraphics& gfx);
	Line& operator=(Line&&) = delete;
	Line(Line&&);
	void AddPoint(DirectX::XMFLOAT3 point, DXGraphics& gfx);
	void SetBrightness(DXGraphics& gfx, float brightness);
	void SetColor(DXGraphics& gfx, Vec4 color);
	const Bind::VertexBuffer& GetVertices() const;
	DirectX::XMMATRIX GetTransformXM() const noexcept override
	{
		return DirectX::XMMatrixIdentity();
	}
	// global moving
	void MoveX(float delta_x);
	void MoveY(float delta_y);
	void MoveZ(float delta_z);
private:
	Bind::PixelConstantBuffer<Brightness> pcbBright;
	Bind::VertexConstantBuffer<CountBuf> vcbCount;
	Bind::PixelConstantBuffer<Color> pcbColor;
	static constexpr float defaultBrightness = 2.0f;
	static constexpr DirectX::XMFLOAT3 defaultRainbowColor = { 1.0f,1.0f,1.0f };
	static constexpr size_t defaultCount = 2u;
	static constexpr size_t cycleLength = 100u;
	size_t points = 0u;
};

class WireFrame : public Drawable
{
public:
	WireFrame(std::wstring modelName, DXGraphics& gfx);
	WireFrame& operator=(WireFrame&&) = delete;
	WireFrame(WireFrame&&)  = delete;
	void SetScale(DXGraphics& gfx,float in_scale)
	{
		vcb.Update(gfx, { in_scale,0.0f,0.0f,0.0f });
	}
	void SetPosition(PositionData pd)
	{
		posData = pd;
	}
	DirectX::XMMATRIX GetTransformXM() const noexcept override
	{
		DirectX::XMMATRIX transMat =
			DirectX::XMMatrixRotationRollPitchYaw(
			posData.rot.x,
			posData.rot.y,
			posData.rot.z) *
			DirectX::XMMatrixTranslation(
			posData.pos.x,
			posData.pos.y,
			posData.pos.z);
		return transMat;
	}
private:
	PositionData posData;
	float scale = 0.01f;
	Bind::VertexConstantBuffer<Vec4> vcb;
};


class Background : public Drawable
{
public:
	Background(DXGraphics& gfx);
	Background& operator=(Background&&) = delete;
	Background(Background&&) = delete;
	DirectX::XMMATRIX GetTransformXM() const noexcept override
	{
		return DirectX::XMMatrixIdentity();
	}
private:
	const PositionData posData{ 0.0f,0.0f,0.0f,0.0f,0.0f,0.0f };
	float scale = 0.5f;
};

class ParticleBurst : public Drawable
{
public:
	ParticleBurst(DXGraphics& gfx, std::vector<DirectX::XMFLOAT3>&& pos);
	ParticleBurst& operator=(ParticleBurst&&);
	ParticleBurst(ParticleBurst&&);
	void UpdatePosition(DXGraphics& gfx, float dt, Vec3 fun(Vec3 pos));
	DirectX::XMMATRIX GetTransformXM() const noexcept override
	{			
		return DirectX::XMMatrixIdentity();
		//return DirectX::XMMatrixTranslation(
		//	posData.pos.x,
		//	posData.pos.y,
		//	posData.pos.z);
	}
private:
	std::vector<DirectX::XMFLOAT3>positions;
	std::vector<Vec3>speeds;
	const size_t totalPoints;
	size_t curPoints;

	//const PositionData posData;

};

class SkyCube : public Drawable
{
public:
	enum class Image
	{
		Red,
		Grey,
		Black,
		Green, 
		Yellow,
		Last
	};
	SkyCube(DXGraphics& gfx, Image img);
	DirectX::XMMATRIX GetTransformXM() const noexcept override
	{
		return DirectX::XMMatrixIdentity();
	}


	std::vector<DirectX::XMFLOAT3>position;
};