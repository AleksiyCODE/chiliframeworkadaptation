#include "Draws.h"
#include "Framework\PixelShader.h"
#include "Framework\VertexShader.h"
#include "Framework\DynamicConstant.h"
#include "Framework\Vertex.h"
#include "Framework\TransformCbuf.h"
#include "Framework\InputLayout.h"
#include "Framework\VertexBuffer.h"
#include "Framework\IndexBuffer.h"
#include "Framework\Topology.h"
#include "CubeTexture.h"
#include "Framework/Sampler.h"
#include "Framework/Rasterizer.h"
#include "Framework/DepthStencil.h"
#include "Framework/Stencil.h"
#include "Cube.h"

Line::Line(DirectX::XMFLOAT3 beg, DirectX::XMFLOAT3 end, DXGraphics& gfx) :
	//rainbowGrad(gfx, RainGrad{ 0 }, 2u),
	pcbBright(gfx, 1u),
	vcbCount(gfx, 4u),
	pcbColor(gfx, 1u)
{
	using namespace Bind;
	using Dvtx::VertexLayout;
	{
		Technique white;	
		Step only(0);
		std::shared_ptr<VertexShader>pvs;
		only.AddBindable(PixelShader::Resolve(gfx, L"PendLinePS.cso"));
		pvs = VertexShader::Resolve(gfx, L"PendLineVS.cso");
		auto pvsbc = pvs->GetBytecode();
		Dvtx::VertexBuffer vbuf(std::move(
			Dvtx::VertexLayout{}
			.Append(VertexLayout::Position3D)
			.Append(VertexLayout::Float3Color)
		));
		vbuf.EmplaceBack(beg, defaultRainbowColor);
		vbuf.EmplaceBack(end, defaultRainbowColor);
		pVertices = std::make_shared<VertexBuffer>(gfx, L"temp", vbuf);			//it is initialised only here
		only.AddBindable(InputLayout::Resolve(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));

		only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_LINELIST));
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));
		white.AddStep(std::move(only));
		techniques.push_back(white);
	}
	{
		Technique centDist;
		Step only(0);
		std::shared_ptr<VertexShader>pvs;
		only.AddBindable(PixelShader::Resolve(gfx, L"CentDistPS.cso"));
		pvs = VertexShader::Resolve(gfx, L"CentDistVS.cso");
		only.AddBindable(std::make_shared<PixelConstantBuffer<Brightness>>(pcbBright));
		auto pvsbc = pvs->GetBytecode();
		Dvtx::VertexBuffer vbuf(std::move(
			Dvtx::VertexLayout{}
			.Append(VertexLayout::Position3D)
			.Append(VertexLayout::Float3Color)
		));
		only.AddBindable(std::make_shared<InputLayout>(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));

		only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_LINELIST));
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));
		centDist.AddStep(std::move(only));
		techniques.push_back(centDist);
	}
	{
		Technique rainbow;
		Step only(0);
		std::shared_ptr<VertexShader>pvs;
		only.AddBindable(PixelShader::Resolve(gfx, L"RainbowPS.cso"));
		pvs = VertexShader::Resolve(gfx, L"RainbowVS.cso");
		auto pvsbc = pvs->GetBytecode();
		Dvtx::VertexBuffer vbuf(std::move(
			Dvtx::VertexLayout{}
			.Append(VertexLayout::Position3D)
			.Append(VertexLayout::Float3Color)
		));
		only.AddBindable(std::make_shared<InputLayout>(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));

		only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_LINELIST));
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));
		rainbow.AddStep(std::move(only));
		techniques.push_back(rainbow);
	}
	{
		Technique crawlingRainbow;
		Step only(0);
		std::shared_ptr<VertexShader>pvs;
		only.AddBindable(PixelShader::Resolve(gfx, L"RainbowPS.cso"));
		pvs = VertexShader::Resolve(gfx, L"CrawlingRainbowVS.cso");
		auto pvsbc = pvs->GetBytecode();
		Dvtx::VertexBuffer vbuf(std::move(
			Dvtx::VertexLayout{}
			.Append(VertexLayout::Position3D)
			.Append(VertexLayout::Float3Color)
		));
		only.AddBindable(std::make_shared<InputLayout>(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));
		only.AddBindable(std::make_shared<VertexConstantBuffer<CountBuf>>(vcbCount));
		only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_LINELIST));
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));
		crawlingRainbow.AddStep(std::move(only));
		techniques.push_back(crawlingRainbow);
	}
	{
		Technique custom;
		Step only(0);
		std::shared_ptr<VertexShader>pvs;
		only.AddBindable(PixelShader::Resolve(gfx, L"CustomPS.cso"));
		pvs = VertexShader::Resolve(gfx, L"PendLineVS.cso");
		auto pvsbc = pvs->GetBytecode();
		Dvtx::VertexBuffer vbuf(std::move(
			Dvtx::VertexLayout{}
			.Append(VertexLayout::Position3D)
			.Append(VertexLayout::Float3Color)
		));
		only.AddBindable(std::make_shared<InputLayout>(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));
		only.AddBindable(std::make_shared<PixelConstantBuffer<Color>>(pcbColor));
		only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_LINELIST));
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));
		custom.AddStep(std::move(only));
		techniques.push_back(custom);
	}
	std::vector<unsigned short> indices;
		indices.push_back(0);
		indices.push_back(1);	
	pIndices = std::make_shared<IndexBuffer>(gfx, L"temp", indices);
	RebindParent();
	points = 2u;
}

Line::Line(DirectX::XMFLOAT3 beg, DXGraphics& gfx):
	Line(beg,beg,gfx)
{}

Line::Line(Line&& mvFrom):
	//rainbowGrad(mvFrom.rainbowGrad),
	pcbBright(mvFrom.pcbBright),
	vcbCount(mvFrom.vcbCount),
	pcbColor(mvFrom.pcbColor)
{
	pIndices = mvFrom.pIndices;
	pVertices = mvFrom.pVertices;
	techniques = std::move(mvFrom.techniques);
	RebindParent();
}

void Line::AddPoint(DirectX::XMFLOAT3 point, DXGraphics& gfx)
{
	static constexpr size_t pointLimit = 80000u;
	pIndices->AddOne(gfx, pointLimit);
	points += 1u;
	pVertices->AddOne(point, DirectX::XMFLOAT3{ (float)points,0.0f,0.0f }, gfx);
	vcbCount.Update(gfx, { (int)points, Vec3{0.0f,0.0f,0.0f } });
}

void Line::SetBrightness(DXGraphics& gfx, float brightness)
{
	pcbBright.Update(gfx, { brightness, Vec3{ 0.0f,0.0f,0.0f } });
}

void Line::SetColor(DXGraphics& gfx, Vec4 color)
{
	pcbColor.Update(gfx, { color });
}

const Bind::VertexBuffer& Line::GetVertices() const
{
	return *Drawable::pVertices;
}

void Line::MoveX(float delta_x)
{
	pIndices->isMoved = 2;
	pVertices->MoveX(delta_x);
}

void Line::MoveY(float delta_y)
{
	pIndices->isMoved = 2;
	pVertices->MoveY(delta_y);
}

void Line::MoveZ(float delta_z)
{
	pIndices->isMoved = 2;
	pVertices->MoveZ(delta_z);
}

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

WireFrame::WireFrame(std::wstring modelName, DXGraphics& gfx)
	: vcb(gfx, { scale,0.0f,0.0f,0.0f }, 3u)
{
	//getting main drawable data
	using namespace Bind;
	namespace dx = DirectX;
	using Dvtx::VertexLayout;
	using namespace std::string_literals;
	Assimp::Importer imp;
	std::string sName = ToNarrow(modelName);
	std::string& pathString = sName;
	const auto pMesh = imp.ReadFile(pathString.c_str(),
		aiProcess_JoinIdenticalVertices |
		aiProcess_ConvertToLeftHanded 
	);
	if (pMesh == nullptr)
	{
		auto s = imp.GetErrorString();	// ModelException not implemented
		assert(false && "bad model");	//throw ModelException(__LINE__, __FILE__, imp.GetErrorString());
	}
	Technique standard;	

		Step only(0);
			
		Dvtx::VertexBuffer vbuf(std::move(
			VertexLayout{}
			.Append(VertexLayout::Position3D)
		));
		std::vector<unsigned short> indices;
		for (unsigned short i = 0u; i < pMesh->mNumMeshes; i++)
		{
			auto mesh = pMesh->mMeshes[i];		//shortcut
			for (unsigned int i = 0; i < mesh->mNumVertices; i++)
			{
				vbuf.EmplaceBack(
					dx::XMFLOAT3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z)
				);
			}
			for (unsigned int j = 0; j < mesh->mNumFaces; j++)
			{
				const auto& face = mesh->mFaces[j];
				indices.push_back(face.mIndices[0]+32u*i);
				indices.push_back(face.mIndices[1]+32u*i);
			}
		}
		only.AddBindable(PixelShader::Resolve(gfx,L"PendLinePS.cso"));
		auto pvs = VertexShader::Resolve(gfx, L"ScaleCentDistVS.cso");
		auto pvsbc = pvs->GetBytecode();
		only.AddBindable(InputLayout::Resolve(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));
		only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_LINELIST));
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));
		
		only.AddBindable(std::make_shared<VertexConstantBuffer<Vec4>>(vcb));

		standard.AddStep(only);			//is not moved to preserve the object

		//these two should be bound outside steps because they stay the same no matter the Technique
		const auto meshTag = modelName + L"%";
		pVertices = VertexBuffer::Resolve(gfx, meshTag, vbuf);
		pIndices = IndexBuffer::Resolve(gfx, meshTag, indices);
	
	AddTechnique(std::move(standard));

	
}

Background::Background(DXGraphics& gfx)
{
	//getting main drawable data
	using namespace Bind;
	namespace dx = DirectX;
	using Dvtx::VertexLayout;
	using namespace std::string_literals;
	Assimp::Importer imp;
	std::string sName = ToNarrow(L"TrueSphere.obj");
	std::string& pathString = sName;
	const auto pMesh = imp.ReadFile(pathString.c_str(),
		aiProcess_Triangulate |
		aiProcess_ConvertToLeftHanded
	);
	if (pMesh == nullptr)
	{
		auto s = imp.GetErrorString();	// ModelException not implemented
		assert(false && "bad model");	//throw ModelException(__LINE__, __FILE__, imp.GetErrorString());
	}
	Technique standard;

	Step only(1);

	Dvtx::VertexBuffer vbuf(std::move(
		VertexLayout{}
		.Append(VertexLayout::Position3D)
	));
	std::vector<unsigned short> indices;
	for (unsigned short i = 0u; i < pMesh->mNumMeshes; i++)
	{
		auto mesh = pMesh->mMeshes[i];		//shortcut
		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			vbuf.EmplaceBack(
				dx::XMFLOAT3(mesh->mVertices[i].x*scale, mesh->mVertices[i].y*scale, mesh->mVertices[i].z*scale)
			);
		}
		for (unsigned int j = 0; j < mesh->mNumFaces; j++)
		{
			const auto& face = mesh->mFaces[j];
			indices.push_back(face.mIndices[0]);
			indices.push_back(face.mIndices[1]);
			indices.push_back(face.mIndices[2]);
		}
	}
	only.AddBindable(PixelShader::Resolve(gfx, L"GradPS.cso"));
	auto pvs = VertexShader::Resolve(gfx, L"GradVS.cso");
	auto pvsbc = pvs->GetBytecode();
	only.AddBindable(InputLayout::Resolve(gfx, vbuf.GetLayout(), pvsbc));
	only.AddBindable(std::move(pvs));
	only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST));
	only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));




	standard.AddStep(only);			//is not moved to preserve the object


	//these two should be bound outside steps because they stay the same no matter the Technique
	const auto meshTag = L"TrueSphere.obj%";
	pVertices = VertexBuffer::Resolve(gfx, meshTag, vbuf);
	pIndices = IndexBuffer::Resolve(gfx, meshTag, indices);

	AddTechnique(std::move(standard));
}

ParticleBurst::ParticleBurst(DXGraphics& gfx, std::vector<DirectX::XMFLOAT3>&& pos)
	:
	totalPoints(pos.size()),
	positions(pos),
	curPoints(totalPoints)
{
	//getting main drawable data
	using namespace Bind;
	namespace dx = DirectX;
	using Dvtx::VertexLayout;

	{
		Technique Particles;
		Step only(0);
		Dvtx::VertexBuffer vbuf(std::move(
			VertexLayout{}
			.Append(VertexLayout::Position3D)
		));
		std::vector<unsigned short> indices;
		indices.reserve(pos.size());
		for (unsigned short i = 0u; i < pos.size(); i++)
		{
			vbuf.EmplaceBack(pos[i]);
			indices.push_back(i);
		}

		only.AddBindable(PixelShader::Resolve(gfx, L"PendLinePS.cso"));
		auto pvs = VertexShader::Resolve(gfx, L"PendLineVS.cso");
		auto pvsbc = pvs->GetBytecode();
		only.AddBindable(InputLayout::Resolve(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));
		only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_POINTLIST));
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));

		Particles.AddStep(only);			//is not moved to preserve the object
		AddTechnique(std::move(Particles));
	}
	{
		Technique Spagetty;
		Step only(0);
		Dvtx::VertexBuffer vbuf(std::move(
			VertexLayout{}
			.Append(VertexLayout::Position3D)
		));
		std::vector<unsigned short> indices;
		indices.reserve(pos.size());
		for (unsigned short i = 0u; i < pos.size(); i++)
		{
			vbuf.EmplaceBack(pos[i]);
			indices.push_back(i);
		}

		only.AddBindable(PixelShader::Resolve(gfx, L"PendLinePS.cso"));
		auto pvs = VertexShader::Resolve(gfx, L"PendLineVS.cso");
		auto pvsbc = pvs->GetBytecode();
		only.AddBindable(InputLayout::Resolve(gfx, vbuf.GetLayout(), pvsbc));
		only.AddBindable(std::move(pvs));
		only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP));
		only.AddBindable(std::make_shared<TransformCbuf>(gfx, TransformCbuf::DataType::Standart));

		Spagetty.AddStep(only);			//is not moved to preserve the object





	pVertices = std::make_shared<VertexBuffer>(gfx, vbuf);
	pIndices = std::make_shared < IndexBuffer>(gfx,  indices);
	AddTechnique(std::move(Spagetty));
	}
	speeds.resize(pos.size(), { 0.0f,0.0f,0.0f });
}

ParticleBurst& ParticleBurst::operator=(ParticleBurst&& mvFrom)
{
	//totalPoints=mvFrom.totalPoints;
	positions=mvFrom.positions;
	curPoints=mvFrom.curPoints;
	pIndices = mvFrom.pIndices;
	pVertices = mvFrom.pVertices;
	speeds = mvFrom.speeds;
	techniques = std::move(mvFrom.techniques);
	RebindParent();
	return *this;
}
ParticleBurst::ParticleBurst(ParticleBurst&& mvFrom)
	:totalPoints(mvFrom.totalPoints),
	positions(mvFrom.positions),
	curPoints(mvFrom.curPoints),
	speeds(mvFrom.speeds)
{
	pIndices = mvFrom.pIndices;
	pVertices = mvFrom.pVertices;
	techniques = std::move(mvFrom.techniques);
	RebindParent();
}

void ParticleBurst::UpdatePosition(DXGraphics& gfx, float dt, Vec3 fun(Vec3 pos))
{
	for (int i = 0; i < curPoints; i++)
	{
		speeds[i] += fun({ positions[i].x, positions[i].y, positions[i].z }) * dt;
		positions[i].x += speeds[i].x * dt;
		positions[i].y += speeds[i].y * dt;
		positions[i].z += speeds[i].z * dt;
	}

	//reloading updated model
	namespace dx = DirectX;
	using Dvtx::VertexLayout;
	using namespace Bind;
	Dvtx::VertexBuffer vbuf(std::move(
		VertexLayout{}
		.Append(VertexLayout::Position3D)
	));
	for (unsigned short i = 0u; i < positions.size(); i++)
	{
		vbuf.EmplaceBack(positions[i]);
	}
	pVertices = std::make_shared<VertexBuffer>(gfx, vbuf);
}

SkyCube::SkyCube(DXGraphics& gfx, Image img)
{
	using namespace Bind;
	using Dvtx::VertexLayout;
	Technique standard;
	Step only(1);
	only.AddBindable(std::make_shared<CubeTexture>(gfx, L"Images\\SpaceBox"+std::to_wstring((size_t)img)));
	only.AddBindable(std::make_shared<TransformCbuf>(gfx));
	only.AddBindable(PixelShader::Resolve(gfx, L"SkyboxPS.cso"));
	only.AddBindable(Topology::Resolve(gfx, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST));
	{ // geometry-related
		auto pvs = Bind::VertexShader::Resolve(gfx, L"SkyboxVS.cso");
		{ // cube
			auto model = Cube::Make();
			const auto geometryTag = L"$cube_map";
			pVertices = VertexBuffer::Resolve(gfx, geometryTag, std::move(model.vertices));
			pIndices = IndexBuffer::Resolve(gfx, geometryTag, std::move(model.indices));
			//count = (UINT)model.indices.size();
			// layout is shared between cube and sphere; use cube data to generate
			only.AddBindable(InputLayout::Resolve(gfx, model.vertices.GetLayout(), pvs->GetBytecode()));
		}
		only.AddBindable(std::move(pvs));
	}
	standard.AddStep(std::move(only));
	AddTechnique(std::move(standard));
}
