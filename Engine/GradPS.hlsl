float4 main(float hight : Hight) : SV_TARGET
{
    float4 col;
    col.x = hight / 500.0f;
    col.y = -hight / 500.0f;
    col.z = 0.08f;
    col.w = 0.2f;
    return float4(col/5.0f);
}