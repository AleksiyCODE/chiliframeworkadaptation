#pragma once
#include "Framework/Bindable.h"

class Surface;

namespace Bind
{
	class OutputOnlyDepthStencil;
	class OutputOnlyRenderTarget;

	class CubeTexture : public Bindable
	{
	public:
		CubeTexture(DXGraphics& gfx, const std::wstring& path, UINT slot = 0);
		void Bind(DXGraphics& gfx) noexcept override;
	private:
		unsigned int slot;
	protected:
		std::wstring path;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> pTextureView;
	};
}