float4 main(float3 col : Color) : SV_TARGET
{    
    return float4(col, 0.7f);
}