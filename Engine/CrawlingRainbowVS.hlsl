#include "Shaders/Transform.hlsl"
#include "Shaders/Scale.hlsl"
#include "Shaders/ShaderOps.hlsl"
struct VSOut
{
    float3 color : Color;
    float4 SVpos : SV_Position;
};

cbuffer VertexCount : register(b4)
{
    int vertexCount;
};

float3 Rainbow(float ratio);


VSOut main(float3 pos : Position, float3 currentVertex : Color)
{
    const unsigned int step = 100;
    VSOut vso;
    unsigned int diff = abs(vertexCount - currentVertex.r);
    unsigned int colorAdvance = diff % step;
    
    
#ifdef SCALE
    pos*=scale;
#endif    
    vso.color = Rainbow((float) colorAdvance / (float) step);
    vso.SVpos = mul(float4(pos, 1.0f), modelViewProj);
    return vso;
}

