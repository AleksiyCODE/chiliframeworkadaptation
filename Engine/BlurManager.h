#pragma once
class BlurManager
{
public:
	static void AddBlur(float blur);	//1.5f - a lot of blur
	void UpdateBlur(float dt);
private:
	float currentBlur = 0.0f;
	static float pendingBlur;
	static constexpr float blurFalloff = 56.5f;
	static constexpr float blurChangeSensitivity = 5.99f;
};

