#include "Porter.h"
#include <fstream>
#include "imgui/imgui.h" 
#include "imgui/imgui_impl_win32.h" 
#include "imgui/imgui_impl_dx11.h" 
#include <algorithm>

void Porter::ParceFile()
{
	loadablePresets.clear();
	try
	{
		bool somethingFound = false;
		std::fstream fs;
		fs.open("savedPresets.txt", std::ofstream::in);
		if (!fs)						//if file does not exist
		{
			throw(1);
		}
		std::string s;
		while (fs)
		{
			fs >> s;
			if (s == entryBegin)
			{
				somethingFound = true;
				std::string ss;
				fs >> ss;
				loadablePresets.push_back(ss);
				while (ss != entryEnd && fs)
				{
					fs >> ss;
				}
				s = entryEnd;
			}
		}
		fs.close();
		if (!somethingFound)
		{
			remove("savedPresets.txt");
			throw(2);
		}
	}
	catch (...)
	{
		failButtonText = "there was no save file\nwe created one for you\ntry saving something";
		displayFailButton = true;
	}
}

bool Porter::LoadParam(size_t position)
{
	size_t currentItem = 0u;
	try
	{
		std::fstream fs;
		fs.open("savedPresets.txt", std::ofstream::in);
		if (!fs)						//if file does not exist
		{
			throw(1);
		}
		std::string s;
		bool loaded = false;
		while (fs &&!loaded)
		{
			fs >> s;
			if (s == entryBegin)
			{
				handsData.clear();
				if (currentItem == position)	//fill in the data
				{
					std::string ss;
					handParam hp;
					fs >> ss;		//drop the name
					fs >> ss;
					comp = static_cast<ComputingMode>(ss[0] - '0');			//only work while there are no 2 digin mods
					fs >> ss;
					tec = static_cast<Techniques>(ss[0] - '0');				//only work while there are no 2 digin tecs
					fs >> ss;
					cum = static_cast<bool>(ss[0] - '0');				//only work while there are no 2 digin tecs
					fs >> ss;			//reading asc in advance
					while (ss != entryEnd && fs)
					{
						std::string::size_type sz;     // alias of size_t
						hp.asc = std::stof(ss, &sz);
						fs >> hp.rot;
						fs >> hp.pit;
						fs >> hp.r;
						fs >> hp.vasc;
						fs >> hp.vrot;
						fs >> hp.vpit;
						handsData.push_back(hp);
						fs >> ss;
					}
					fs.close();
					if (CheckCurrentData())			//bad data in file
						throw(2);
					loaded = true;
				} 
				else
				{
					currentItem++;
				}
			}
		}
	}
	catch (...)
	{
		displayFailButton = true;
		failButtonText = "something broke\nnow all saves are deleted";
		loadablePresets.clear();
		remove("savedPresets.txt");
		return true;
	}
	loadablePresets.clear();
	return false;
}

bool Porter::SaveParameters(const std::string& exportName)
{
	if (!exportName.empty())
	{
		boolean goodString = true;
		for (int i = 0; i < strlen(exportName.c_str()) && goodString; i++)
		{
			if (goodString && !((exportName[i] <= 'z' && exportName[i] >= 'a') || (exportName[i] <= 'Z' && exportName[i] >= 'A')|| (exportName[i] <= '9' && exportName[i] >= '0')))
				goodString = false;
		}
		if (goodString)
		{
			std::string name = exportName;
			name.erase(remove_if(name.begin(), name.end(), isspace), name.end());	//removing spaces
			std::fstream fs;
			fs.open("savedPresets.txt", std::ofstream::out | std::ofstream::app);
			fs << entryBegin << std::endl;
			fs << name << std::endl;
			fs << static_cast<int>(comp) << std::endl;
			fs << static_cast<int>(tec) << std::endl;
			fs << static_cast<int>(cum) << std::endl;
			for (auto& el : handsData) fs << el;
			fs << entryEnd << std::endl;
			fs.close();
			ParceFile();
			return false;
		}
		else
		{
			displayFailButton = true;
			failButtonText = "only english and\ndigits, please";
			return true;
		}
	}
	else
	{
		displayFailButton = true;
		failButtonText = "Don't forget to\n enter the name";
		return true;
	}
}

bool Porter::CheckCurrentData()
{
	bool badFile = false;
	if (comp >= ComputingMode::Last || comp < static_cast<ComputingMode>(0)) badFile = true;
	if (tec >= Techniques::Last || tec <static_cast<Techniques>(0)) badFile = true;
	if (handsData.size() > maxHands || handsData.size() < minHands) badFile = true;
	for (const auto& hand : handsData)
	{
		if (hand.asc > maxReasonable || hand.asc < minReasonable) badFile = true;
		if (hand.rot > maxReasonable || hand.rot < minReasonable) badFile = true;
		if (hand.pit > maxReasonable || hand.pit < minReasonable) badFile = true;
		if (hand.r > maxReasonable || hand.r < 0) badFile = true;
		if (hand.vasc > maxReasonable || hand.vasc < minReasonable) badFile = true;
		if (hand.vrot > maxReasonable || hand.vrot < minReasonable) badFile = true;
		if (hand.vpit > maxReasonable || hand.vpit < minReasonable) badFile = true;
	}
	if (badFile)
	{
		RestoreZeroData();
		remove("savedPresets.txt");
	}
		return badFile;
}

void Porter::RestoreZeroData()
{
	comp = ComputingMode::Classic;
	tec = Techniques::Lighted;
}

Porter::Porter(ComputingMode& comp, Techniques& tec, bool& cum, std::vector<handParam>& handsData):
	comp(comp),
	tec(tec),
	cum(cum),
	handsData(handsData)
{}

Porter::Result Porter::SpawnWindow()
{
	Result res = Result::Idle;
	if (ImGui::Begin("Import / Export"))
	{
		ImGui::Text("Name for export");
		ImGui::InputText("", inputBuffer, IM_ARRAYSIZE(inputBuffer));
		if (ImGui::Button("Save parameters"))
		{
			if(!SaveParameters(inputBuffer))		//if saved
				displaySuccessButton = true;
		}
		if (ImGui::Button("Load parameters"))
		{
			ParceFile();
		}
	}
	ImGui::End();
	if (displaySuccessButton)
	{
		if (ImGui::Begin("Success"))
		{
			if (ImGui::Button("oke"))
			{
				displaySuccessButton = false;
			}
		}
		ImGui::End();
	}
	if (displayFailButton)
	{
		if (ImGui::Begin("Failed"))
		{
			ImGui::Text(failButtonText.c_str());
			if (ImGui::Button("oke"))
			{
				displayFailButton = false;
			}
		}
		ImGui::End();
	}
	if (loadablePresets.size())
	{
		if (ImGui::Begin("Choose what to load"))
		{
			for (size_t i = 0u; i < loadablePresets.size();i++)
			{
				if (ImGui::Button(loadablePresets[i].c_str()))
				{
					if (LoadParam(i))
						res = Result::LoadFail;
					else
						res = Result::LoadSuccess;
				}
			}
		}
		ImGui::End();
	}
	return res;
}
