cbuffer Brightness : register(b1)
{
    float brightness;
};
float4 main(float dist : CentDist) : SV_TARGET
{
    float grad = brightness / (dist*dist + 1.0f);
    return float4(grad, grad, grad, grad);
}