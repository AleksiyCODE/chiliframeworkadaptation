#include "Shaders/Transform.hlsl"
#include "Shaders/Scale.hlsl"
#include "Shaders/ShaderOps.hlsl"
struct VSOut
{
    float3 color : Color;
    float4 SVpos : SV_Position;
};

VSOut main(float3 pos : Position, float3 currentVertex : Color)
{
    unsigned int rainbowStep = 400u;
    VSOut vso;
#ifdef SCALE
    pos*=scale;
#endif    
    vso.color = Rainbow(float(uint(currentVertex.r) % rainbowStep) / float(rainbowStep));
    vso.SVpos = mul(float4(pos, 1.0f), modelViewProj);
    return vso;
}