float4 main(float closeness : Closeness) : SV_TARGET
{
    return float4(closeness + 0.1f, closeness + 0.1f, closeness + 0.1f, closeness + 0.1f);
}