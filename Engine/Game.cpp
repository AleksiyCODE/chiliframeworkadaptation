#include "Game.h" 
#include "imgui/imgui.h" 
#include "imgui/imgui_impl_win32.h" 
#include "imgui/imgui_impl_dx11.h" 
#include "RandomUnit.h" 
#include <cmath> 
#include <math.h> 
#include <random> 
#include "Framework\Mat.h"
#include <fstream>

Game::Game() :
	wnd(L"Rifted Times"),
	fc(wnd.DXGfx()),
	burM(wnd.DXGfx()),
	handEnd(L"Sphere.obj", wnd.DXGfx()),
	//bcg(wnd.DXGfx()),
	por(computingMode, traceDrawingTec, isStackingCompution, handsData)
{
	trace = std::make_unique<Line>(DirectX::XMFLOAT3{ 0.0f,0.0f,0.0f }, wnd.DXGfx());
	ResetTrace();
	currentShadeMode = shadeModeLables[1];
	currentComputingMode = computingModeLables[0];
	wnd.DXGfx().SetProjection(DirectX::XMMatrixPerspectiveLH(0.1f, 0.1f * 9.0f / 16.0f, 0.1f, 500.0f));
	cam.Reset();
	ResetHands();
	FrameCommander::SetBlurMod(255.0f);	//does not matter
	badTrace = true;			//avoids artifact 
	sky = std::make_unique<SkyCube>(wnd.DXGfx(), background);
}

int Game::Go()
{
	while (true)
	{

		if (isExiting)break;
		HandleInput();
		// process all messages pending, but to not block for new messages 
		if (const auto ecode = MainWindow::ProcessMessages())
		{
			// if return optional has value, means we're quitting so return exit code 
			return *ecode;
		}
		dt = timer.Mark() * speed_factor;

		MoveHands();		//and add trace segment 

		wnd.DXGfx().BeginFrame(0.0f, 0.0f, 0.0f);
		if(currentCamMode==camModeLables[2])		//if camera needs smoothing
			cam.Update();
		wnd.DXGfx().SetCamera(cam.GetMatrix());
		wnd.DXGfx().SetCamPos(cam.GetPosition());

		if (badTrace)
		{
			ResetTrace();
			badTrace = false;
		}
		//cam.SpawnControlWindow();
		SpawnControlWindow();		//general window 

		if(backgroundEnabled) sky->Submit(fc);
		trace->Submit(fc, traceDrawingTec);
		
		if (currentCamMode != camModeLables[1])
		{
			for (auto& l : hands)l->Submit(fc, Techniques::White);
			if (indicatorBallDisplayed)
			{
				handEnd.Submit(fc, Techniques::White);
			}
		}
		//bcg.Submit(fc, Techniques::White);
		burM.UpdateBursts(dt*speed, wnd.DXGfx());
		burM.SubmitBursts(fc);

		fc.Execute(wnd.DXGfx(), dt);
		wnd.DXGfx().EndFrame();
		fc.Reset();

		hands.clear();
	}
	return(-1);					//for compiler not to worry 
}

void Game::SpawnControlWindow()
{
	UpdateGUISelectables();	
		
	if (ImGui::Begin("Simulation Control"))
	{
		ImGui::SliderFloat("Simulation Speed", &speed, 0.0f, speedLimit);
		if (ImGui::Button("Add One Hand"))
		{
			AddHand();
		}
		if (ImGui::Button("Randomize Parameters")) {
			RandomizeHands();
			badTrace = true;
		}
		ImGui::PushStyleColor(ImGuiCol_Button, { 0.9f,0.9f,0.9f,1.0f });
		ImGui::PushStyleColor(ImGuiCol_Text, { 0.1f,0.1f,0.1f,1.0f });
		if (ImGui::Button("Clear Trace"))
		{
			badTrace = true;
		}
		ImGui::PopStyleColor();
		ImGui::PopStyleColor();

		ImGui::PushStyleColor(ImGuiCol_Button, { 0.9f,0.54f,0.0f,1.0f });
		if (ImGui::Button("Fast Forward Drawing"))
		{
			if (speed != NEAR_ZERO)
				fastForwarding = true;
		}
		ImGui::PopStyleColor();

		ImGui::PushStyleColor(ImGuiCol_Button, { 1.0f,0.3f,0.3f,1.0f });
		if (ImGui::Button("DESINTEGRATE"))
		{
			burM.InitiateBurstFromTrail(trace->GetVertices(), BurstManager::Type::Particle);
			ResetTrace();
		}
		ImGui::PopStyleColor();
		ImGui::PushStyleColor(ImGuiCol_Button, { 0.1f,0.6f,0.1f,1.0f });
		if (ImGui::Button("SPAGETTYFY"))
		{
			burM.InitiateBurstFromTrail(trace->GetVertices(),BurstManager::Type::Spagetty);
			ResetTrace();
		}
		ImGui::PopStyleColor();
	}
	ImGui::End();

	if (ImGui::Begin("Advanced"))
	{
		if (ImGui::BeginCombo("Computing Mode", currentComputingMode)) // The second parameter is the label previewed before opening the combo. 
		{
			for (int n = 0; n < IM_ARRAYSIZE(computingModeLables); n++)
			{
				bool is_selected = (currentComputingMode == computingModeLables[n]); // You can store your selection however you want, outside or inside your objects 
				if (ImGui::Selectable(computingModeLables[n], is_selected))
				{
					badTrace = true;		//clear trace due to change in formula
					currentComputingMode = computingModeLables[n];
					if (currentComputingMode == computingModeLables[0])
						computingMode = ComputingMode::Classic;
					else
						if (currentComputingMode == computingModeLables[1])
							computingMode = ComputingMode::Uniform;
						else
							if (currentComputingMode == computingModeLables[2])
								computingMode = ComputingMode::Parallel;
							else
								if (currentComputingMode == computingModeLables[3])
									computingMode = ComputingMode::Meredian;
								else
									if (currentComputingMode == computingModeLables[4])
										computingMode = ComputingMode::Wavy;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support) 
			}
			ImGui::EndCombo();
		}


		bool stackTemp = isStackingCompution;
		if (stackTemp)
			ImGui::PushStyleColor(ImGuiCol_Button, { 1.0f,0.3f,0.3f,1.0f });
		if (ImGui::Button("Toggle cumulative compution")) {
			if (isStackingCompution)
			{
				isStackingCompution = false;
			}
			else
			{
				isStackingCompution = true;
			}
			badTrace = true;
		}
		if (stackTemp)
			ImGui::PopStyleColor();

		bool overTemp = overDriven;
		if (overTemp)
			ImGui::PushStyleColor(ImGuiCol_Button, { 1.0f,0.3f,0.3f,1.0f });
		if (ImGui::Button("Toggle Overdrive Mode")) {
			if (overDriven)
			{
				overDriven = false;
				maxHandSpeed = maxNormalHandSpeed;
				minHandSpeed = minNormalHandSpeed;
				speedLimit = normalSpeedLimit;
			}
			else
			{
				overDriven = true;
				maxHandSpeed = maxOverdriveHandSpeed;
				minHandSpeed = minOverdriveHandSpeed;
				speedLimit = overdriveSpeedLimit;
			}
		}
		if (overTemp)
			ImGui::PopStyleColor();

		ImGui::PushStyleColor(ImGuiCol_Button, { 0.3f,0.3f,0.1f,1.0f });
		if (ImGui::Button("Translate left")) {
			MoveX(FORW_STEP_SIZE);
		}
		if (ImGui::Button("Translate right")) {
			MoveX(BACK_STEP_SIZE);
		}
		if (ImGui::Button("Translate up")) {
			MoveY(FORW_STEP_SIZE);
		}
		if (ImGui::Button("Translate down")) {
			MoveY(BACK_STEP_SIZE);
		}
		if (ImGui::Button("Translate forwards")) {
			MoveZ(FORW_STEP_SIZE);
		}
		if (ImGui::Button("Translate backwards")) {
			MoveZ(BACK_STEP_SIZE);
		}
		ImGui::PopStyleColor();
	}
	ImGui::End();
	if (ImGui::Begin("Hand Parameters"))
	{
		for (int i = 0; i < handsData.size(); i++) {
			std::string str;
			str = "Hand " + std::to_string(i+1) + " configuration";
			ImGui::Text(str.c_str());

			ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.44f, 0.44f, 0.54f, 1.00f));
			str = "Length " + std::to_string(i + 1);
			ImGui::SliderFloat(str.c_str(), &(handsData[i]).r, minHandLength, maxHandLength, "%.1f");
			ImGui::PopStyleColor();

			ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.44f, 0.54f, 0.44f, 1.00f));
			str = "Ascension Speed " + std::to_string(i + 1);
			ImGui::SliderFloat(str.c_str(), &handsData[i].vasc, minHandSpeed, maxHandSpeed, "%.1f");
			str = "Rotation Speed " + std::to_string(i + 1);
			ImGui::SliderFloat(str.c_str(), &(handsData[i].vrot), minHandSpeed, maxHandSpeed, "%.1f");
			if (computingMode == ComputingMode::Parallel || computingMode == ComputingMode::Meredian || computingMode == ComputingMode::Wavy)
			{
				str = "Pitch Speed " + std::to_string(i + 1);
				ImGui::SliderFloat(str.c_str(), &(handsData[i].vpit), minHandSpeed, maxHandSpeed, "%.1f");
			}
			ImGui::PopStyleColor();
			if (ImGui::Button("Delete this hand")) {
				RemoveHand(i);
			}
		}
	}
	ImGui::End();
	if (ImGui::Begin("Display params"))
	{
		ImGui::Text("USE ESC TO SWITCH BETWEEN");
		ImGui::Text("USING CURSOR AND CONTROLLING");
		ImGui::Text("THE CAMERA");
		if (ImGui::BeginCombo("Camera Mode", currentCamMode)) // The second parameter is the label previewed before opening the combo. 
		{
			for (int n = 0; n < IM_ARRAYSIZE(camModeLables); n++)
			{
				bool is_selected = (currentCamMode == camModeLables[n]); // You can store your selection however you want, outside or inside your objects 
				if (ImGui::Selectable(camModeLables[n], is_selected))
				{
					currentCamMode = camModeLables[n];
					if (currentCamMode == camModeLables[0])
						cam.Reset();

					// set spheric mode
					if (currentCamMode == camModeLables[2]) {
						cam.Reset();
						cam.SetDefaultSpheric();
					}
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support) 
			}
			ImGui::EndCombo();
		}
		if (ImGui::BeginCombo("Shading Mode", currentShadeMode)) // The second parameter is the label previewed before opening the combo. 
		{
			for (int n = 0; n < IM_ARRAYSIZE(shadeModeLables); n++)
			{
				bool is_selected = (currentShadeMode == shadeModeLables[n]); // You can store your selection however you want, outside or inside your objects 
				if (ImGui::Selectable(shadeModeLables[n], is_selected))
				{
					currentShadeMode = shadeModeLables[n];
					if (currentShadeMode == shadeModeLables[0])
						traceDrawingTec = Techniques::White;
					else
						if (currentShadeMode == shadeModeLables[1])
							traceDrawingTec = Techniques::Lighted;
						else
							if (currentShadeMode == shadeModeLables[2])
								traceDrawingTec = Techniques::Rainbow;
							else
								if (currentShadeMode == shadeModeLables[3])
									traceDrawingTec = Techniques::CrawlingRainbow;
								else
									if (currentShadeMode == shadeModeLables[4])
										traceDrawingTec = Techniques::Custom;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support) 
			}
			ImGui::EndCombo();
		}
		if (currentShadeMode == shadeModeLables[1])
			ImGui::SliderFloat("brightness", &brightness, 0.0f, 20.0f, "%.1f");
		if (currentShadeMode == shadeModeLables[4])		//if custimisable
		{
			ImGui::ColorPicker4("TraceColor", shaderColor, 0);
		}
		ImGui::Checkbox("Display Hands", &handsDisplayed);
		ImGui::Checkbox("Display Ball Indicator", &indicatorBallDisplayed);
		if (ImGui::Button("Change Background"))
		{
			if (background == SkyCube::Image::Last)
			{
				background = SkyCube::Image::Red;
				backgroundEnabled = true;
			}
			else
				background = (SkyCube::Image)((size_t)background + 1);
			if (background == SkyCube::Image::Last)
				backgroundEnabled = false;
			else
				sky = std::make_unique<SkyCube>(wnd.DXGfx(), background);
		}
	}
	ImGui::End();
	switch (por.SpawnWindow())
	{
	case(Porter::Result::LoadSuccess):
		badTrace = true;
		break;
	case(Porter::Result::LoadFail):
		ResetHands();
		break;
	default:
		break;
	}
}
void Game::UpdateGUISelectables()
{
	switch (traceDrawingTec)
	{
	case Techniques::White:
		currentShadeMode = shadeModeLables[0];
		break;
	case Techniques::Lighted:
		currentShadeMode = shadeModeLables[1];
		break;
	case Techniques::Rainbow:
		currentShadeMode = shadeModeLables[2];
		break;
	case Techniques::CrawlingRainbow:
		currentShadeMode = shadeModeLables[3];
		break;
	case Techniques::Custom:
		currentShadeMode = shadeModeLables[4];
		break;
	case Techniques::Last:
		break;
	default:
		break;
	}
	switch (computingMode)
	{
	case ComputingMode::Classic:
		currentComputingMode = computingModeLables[0];
		break;
	case ComputingMode::Uniform:
		currentComputingMode = computingModeLables[1];
		break;
	case ComputingMode::Parallel:
		currentComputingMode = computingModeLables[2];
		break;
	case ComputingMode::Meredian:
		currentComputingMode = computingModeLables[3];
		break;
	case ComputingMode::Wavy:
		currentComputingMode = computingModeLables[4];
		break;
	case ComputingMode::Last:
		break;
	default:
		break;
	}
	
}


void Game::MoveHands()
{
	std::vector<DirectX::XMFLOAT3> p;
	size_t simulationCycles = 1u;
	if (fastForwarding)
	{
		if (speed != 0.0f)
			simulationCycles = fastForwardCycles;
		fastForwarding = false;
	}
	for (size_t i = 0; i < simulationCycles; i++)
	{
		p.clear();
		p.push_back({ 0.0f,0.0f,0.0f });
		for (int i = 0; i < handsData.size(); i++) 
		{
			p.push_back(ComputePoint(i,p[i]));
		}
		if (speed != 0.0f)
		{
			trace->AddPoint(p.back(), wnd.DXGfx());
		}
		{
			const float dt = (simulationCycles != 1u || Game::dt > 1.0f / 10.0f) ? 1.0f / 60.0f : Game::dt;
			for (int i = 0; i < handsData.size(); i++)
			{

				float vasc = 0.0f, vrot = 0.0f, vpit = 0.0f;
				if (isStackingCompution)
				{
					for (int j = i; j >= 0; j--)
					{					
						vasc += handsData[j].vasc;
						vrot += handsData[j].vrot;
						vpit += handsData[j].vpit;
					}
				}
				else
				{
					vasc = handsData[i].vasc;
					vrot = handsData[i].vrot;
					vpit = handsData[i].vpit;
				}
				handsData[i].asc = wrap_angle(handsData[i].asc + speed * vasc * dt);
				handsData[i].rot = wrap_angle(handsData[i].rot + speed * vrot * dt);
				handsData[i].pit = wrap_angle(handsData[i].pit + speed * vpit * dt);
			}
		}
	}
	trace->SetBrightness(wnd.DXGfx(), brightness);
	trace->SetColor(wnd.DXGfx(), { shaderColor[0],shaderColor[1],shaderColor[2],shaderColor[3] });
	if (currentCamMode == camModeLables[1])		//rolling the camera 
	{
		if (speed != 0.0f)
		{
			Vec3 diff = (Vec3{ p[p.size() - 1] } - lastPointAddedToTrace).Normalize();
			cam.SetBaseDirection(diff);
			cam.SetFullTransform({ {0.0f, 0.0f, 0.0f}, {p[p.size() - 1].x, p[p.size() - 1].y, p[p.size() - 1].z} });
		}
	}
	if (handsDisplayed)
	{
		for (int i = 1; i < p.size(); i++)
		{
			hands.emplace_back(std::make_unique<Line>(p[i - 1], p[i], wnd.DXGfx()));
		}
	}
	lastPointAddedToTrace = p[p.size() - 1];
	if (indicatorBallDisplayed)
	{
		handEnd.SetPosition({ p[p.size() - 1].x, p[p.size() - 1].y, p[p.size() - 1].z, 0.0f, 0.0f, 0.0f });
		if (handEndScale > handEndDefScale)handEndScale *= RU.GetFloat(-handEndScaleChange + 1.0f, 1.0f); else handEndScale *= RU.GetFloat(1.0f, handEndScaleChange + 1.0f);
	}
	handEnd.SetScale(wnd.DXGfx(), handEndScale * (handsData[1].r + handsData[0].r));
}
void Game::ResetHands()
{
	handsData.clear();
	handsData.emplace_back(0.0f, 0.0f, 0.0f, 1.5f, 1.1f, 1.4f, 0.0f);
	handsData.emplace_back(0.0f, 0.0f, 0.0f, 1.5f, 3.2f, 2.3f, 0.0f);
};
void Game::ResetTrace()
{
	trace = std::make_unique<Line>(DirectX::XMFLOAT3{ lastPointAddedToTrace.x,lastPointAddedToTrace.y,lastPointAddedToTrace.z }, wnd.DXGfx());
	handEnd.SetPosition({ lastPointAddedToTrace.x,lastPointAddedToTrace.y,lastPointAddedToTrace.z, 0.0f, 0.0f, 0.0f });
}

void Game::HandleInput()
{
	while (const auto e = wnd.kbd.ReadKey())
	{
		if (!e->IsPress())			//no need to handle non-press events 
		{
			continue;
		}
		switch (e->GetCode())
		{
		case VK_ESCAPE:
			if (wnd.CursorEnabled())
			{
				wnd.DisableCursor();
				wnd.mouse.EnableRaw();
			}
			else
			{
				wnd.EnableCursor();
				wnd.mouse.DisableRaw();
			}
			break;
		case VK_SPACE:
			if (speed == 0.0f || speed == NEAR_ZERO)
			{
				speed = cachedSpeed;
				cachedSpeed = 0.0f;
			}
			else
			{
				cachedSpeed = speed;
				speed = 0.0f;
			}
			break;
		case 0x51:
			PostQuitMessage(0);
		case VK_F1:
			showDemoWindow = true;
			break;
		}
	}

	if (currentCamMode == camModeLables[0])
	{
		if (!wnd.CursorEnabled())
		{
			if (wnd.kbd.KeyIsPressed('W'))
			{
				cam.Translate({ 0.0f,0.0f,dt });
			}
			if (wnd.kbd.KeyIsPressed('A'))
			{
				cam.Translate({ -dt,0.0f,0.0f });
			}
			if (wnd.kbd.KeyIsPressed('S'))
			{
				cam.Translate({ 0.0f,0.0f,-dt });
			}
			if (wnd.kbd.KeyIsPressed('D'))
			{
				cam.Translate({ dt,0.0f,0.0f });
			}
			if (wnd.kbd.KeyIsPressed('R'))
			{
				cam.Translate({ 0.0f,dt,0.0f });
			}
			if (wnd.kbd.KeyIsPressed('F'))
			{
				cam.Translate({ 0.0f,-dt,0.0f });
			}
		}

		while (const auto delta = wnd.mouse.ReadRawDelta())
		{
			if (!wnd.CursorEnabled())
			{
				cam.Rotate((float)delta->x, (float)delta->y);
			}
		}
	}
	else
		if (currentCamMode == camModeLables[2])
		{
			if (!wnd.CursorEnabled())
			{
				if (wnd.kbd.KeyIsPressed('W'))
				{
					cam.SphericMoveUpDown(-dt);
				}
				if (wnd.kbd.KeyIsPressed('A'))
				{
					cam.SphericRotate(-dt);
				}
				if (wnd.kbd.KeyIsPressed('S'))
				{
					cam.SphericMoveUpDown(dt);
				}
				if (wnd.kbd.KeyIsPressed('D'))
				{
					cam.SphericRotate(dt);
				}
				if (wnd.kbd.KeyIsPressed('R'))
				{
					cam.SphericZoom(-dt);
				}
				if (wnd.kbd.KeyIsPressed('F'))
				{
					cam.SphericZoom(dt);
				}
				if (wnd.mouse.wheelIsUp)
				{
					cam.SphericZoom(-dt * 20.0f);
				}
				if (wnd.mouse.wheelIsDown)
				{
					cam.SphericZoom(dt * 20.0f);
				}
			}
			while (const auto delta = wnd.mouse.ReadRawDelta())
			{
				if (!wnd.CursorEnabled())
				{
					cam.SphereSlide((float)delta->x, (float)delta->y);
				}
			}
		}
}

void Game::AddHand()
{
	if (!(handsData.size() >= 10)) {
		handsData.push_back({ 0.0f, 0.0f, 0.0f, 0.5f, 0.1f, 0.1f, 0.1f });
	}
}

void Game::RemoveHand(int i)
{
	if (handsData.size() > 2) 
	{
		handsData.erase(handsData.begin() + i);
	}
}

void Game::RandomizeHands()
{
	for (int i = 0; i < handsData.size(); i++) 
	{
		float rot = RU.GetFloat(0.0f, PI);
		float asc = RU.GetFloat(0.0f, PI);
		float pit = RU.GetFloat(0.0f, PI);
		float len = float(RU.GetUInt(size_t(minHandLength * 10.0f), size_t(maxHandLength * 10.0f)))/ 10.0f;
		float vasc = float(RU.GetInt(int(minHandSpeed * 10.0f), int(maxHandSpeed * 10.0f))) / 10.0f;
		float vrot = float(RU.GetInt(int(minHandSpeed * 10.0f), int(maxHandSpeed * 10.0f))) / 10.0f;
		float vpit = float(RU.GetInt(int(minHandSpeed * 10.0f), int(maxHandSpeed * 10.0f))) / 10.0f;
		switch (RU.GetUInt(0u,8u))			//some breaks are skippe on purpose
		{
		case(0u):
		{
			vpit = maxHandSpeed;
			modf(vasc, &vasc);
			break;
		}
		case(1u):
		{
			vpit = 0.1f;
			modf(vrot, &vrot);
		}
		case(2u):
		{
			vasc = RU.GetBool() ? 0.1f : -0.2f;
			break;
		}
		case(3u):
		{
			vpit = RU.GetBool() ? -0.1f : 0.2f;
			vrot = RU.GetBool() ? 0.1f : 0.2f;
		}
		case(4u):
		{
			vasc = RU.GetBool() ? 0.0f : std::clamp(vrot * 2.0f, minHandSpeed, maxHandSpeed);
			vrot = maxHandSpeed;
			break;
		}
		case(5u):
		{
			vpit = 0.0f;
			vrot = RU.GetBool() ? 0.0f : std::clamp(vasc * -3.0f, minHandSpeed, maxHandSpeed);
			break;
		}
		case(6u):
		{
			vrot = 0.0f;
			vasc = maxHandSpeed;
		}
		case(7u):
		{
			vasc = 1.0f;
			modf(vrot, &vrot);
		}
		case(8u):
		{
			vrot = maxHandSpeed;
			modf(vpit, &vrot);
			break;
		}

		default:
			break;
		}
		handsData[i] = { asc, rot, pit, len, vasc, vrot, vpit };
	}
	MoveHands();
}

void Game::ChangeBackground()
{

}

DirectX::XMFLOAT3 Game::ComputePoint(size_t i, DirectX::XMFLOAT3 previousPoint)
{
	switch (computingMode)
	{
	case(ComputingMode::Classic):
		return{ sin(handsData[i].asc) * cos(handsData[i].rot) * handsData[i].r + previousPoint.x,-cos(handsData[i].asc) * handsData[i].r + previousPoint.y, sin(handsData[i].asc) * sin(handsData[i].rot) * handsData[i].r + previousPoint.z };
	case(ComputingMode::Uniform):
		return{ sin(handsData[i].asc)* cos(handsData[i].rot)* handsData[i].r + previousPoint.x, -cos(handsData[i].asc) * sin(handsData[i].rot) * handsData[i].r + previousPoint.y, sin(handsData[i].asc)* sin(handsData[i].rot)* handsData[i].r + previousPoint.z };
	case(ComputingMode::Parallel):
	{
		//asc - around x
		//rot - around y
		//pit - aroundz
		Vec3 rotComponent{ cos(handsData[i].rot),  0.0f,sin(handsData[i].rot) };
		Vec3 ascComponent{ 0.0f,  cos(handsData[i].asc), sin(handsData[i].asc) };
		Vec3 pitComponent{ cos(handsData[i].pit),  sin(handsData[i].pit), 0.0f };
		Vec3 endPoint = (rotComponent + ascComponent + pitComponent) * handsData[i].r / 3.0f + previousPoint;
		return  { endPoint.x,endPoint.y,endPoint.z };
	}
	case(ComputingMode::Meredian):
	{
		Vec3 circle{ cos(handsData[i].rot),  0.0f,sin(handsData[i].rot) };
		circle *= (Mat3::RotationX(handsData[i].asc)* Mat3::RotationZ(handsData[i].pit));
		Vec3 endPoint = circle * handsData[i].r + previousPoint;
		return { endPoint.x,endPoint.y,endPoint.z };
	}
	case(ComputingMode::Wavy):
	{
		return{ sin(handsData[i].asc) * handsData[i].r + previousPoint.x,sin(handsData[i].rot) * handsData[i].r + previousPoint.y,  sin(handsData[i].pit) * handsData[i].r + previousPoint.z };
	}
	default:
		return DirectX::XMFLOAT3();
	}

}

// global moving

void Game::MoveX(float delta_x) {
	if (speed != NEAR_ZERO) {
		cachedSpeed = speed;
	}
	speed = NEAR_ZERO;
	trace->MoveX(delta_x);
}

void Game::MoveY(float delta_y) {
	if (speed != NEAR_ZERO) {
		cachedSpeed = speed;
	}
	speed = NEAR_ZERO;
	trace->MoveY(delta_y);
}

void Game::MoveZ(float delta_z) {
	if (speed != NEAR_ZERO) {
		cachedSpeed = speed;
	}
	speed = NEAR_ZERO;
	trace->MoveZ(delta_z);
}