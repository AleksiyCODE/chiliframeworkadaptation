#pragma once
#include "./Framework/MainWindow.h"
#include "imgui/ImguiManager.h"
#include "./Framework/FrameTimer.h"
#include "Framework/FrameCommander.h"
#include"./Framework/TextDrawer.h"
#include "BlurManager.h"
#include "Draws.h"
#include "Framework\Camera.h"
#include "Framework\PlaneTexNorm.h"
#include "Structures.h"
#include "Porter.h"
#include "BurstManager.h"


#define FORW_STEP_SIZE 1.0f
#define BACK_STEP_SIZE -1.0f
#define NEAR_ZERO 0.000001f

class Game
{
public:
	Game();
	Game(const Game&) = delete;
	Game& operator=(const Game&) = delete;
	int Go();



private:
	void SpawnControlWindow();
	void UpdateGUISelectables();			//if selectables are modifyed externally (ei by inmporting) this keeps track
	void MoveHands();
	void ResetHands();
	void ResetTrace();
	void HandleInput();

	void AddHand();
	void RemoveHand(int i);
	void RandomizeHands();
	void ChangeBackground();
	DirectX::XMFLOAT3 ComputePoint(size_t i, DirectX::XMFLOAT3 previousPoint);

	// global moving
	void MoveX(float delta_x);
	void MoveY(float delta_y);
	void MoveZ(float delta_z);

	ImguiManager imgui;
	MainWindow wnd;
	FrameTimer timer;
	float speed_factor = 1.0f;
	float dt = 0.0f;
	FrameCommander fc;
	bool showDemoWindow = true;	
	bool isExiting = false;
	//BlurManager bm;
	Camera cam;


	BurstManager burM;

	std::vector<std::unique_ptr<Line>> hands;
	//Background bcg;
	
	std::unique_ptr<Line> trace;
	bool badTrace = false;
	
	std::vector<handParam> handsData;
	float speed = 0.5f;
	float cachedSpeed = 0.5f;
	
	WireFrame handEnd;
	static constexpr float handEndDefScale = 0.02f;
	float handEndScale = 0.02f;
	float handEndScaleChange = 0.33f;
	bool handsDisplayed = true;
	bool indicatorBallDisplayed = true;
	
	float brightness = 8.0f;
	SkyCube::Image background = SkyCube::Image::Red;
	bool backgroundEnabled = true;
	std::unique_ptr<SkyCube> sky;

	Vec3 lastPointAddedToTrace{ 0.0f,0.0f,0.0f };
	
	const char* camModeLables[3] = { "Free Camera", "Rolling Camera", "Spheric Camera" };
	const char* currentCamMode = camModeLables[2];
	
	
	
	const char* currentShadeMode = nullptr;
	const char* shadeModeLables[5] = { "Uniform", "Lighten", "Rainbow", "Crawling Rainbow", "Custom" };
	Techniques traceDrawingTec = Techniques::Lighted;
	float shaderColor[4]{ 1.0f,1.0f,1.0f,1.0f };
	
	const char* currentComputingMode = nullptr;
	const char* computingModeLables[5] = { "Classic", "Uniform", "Parallel", "Meredian", "Wavy" };
	ComputingMode computingMode = ComputingMode::Classic;
	
	bool isStackingCompution = false;
	
	bool fastForwarding = false;
	static constexpr size_t fastForwardCycles = 5000u;
	
	static constexpr float maxHandLength = 3.0f;
	static constexpr float minHandLength = 0.5f;
	float maxHandSpeed = maxNormalHandSpeed;
	float minHandSpeed = minNormalHandSpeed;
	static constexpr float maxNormalHandSpeed = 4.0f;
	static constexpr float minNormalHandSpeed = -4.0f;
	static constexpr float maxOverdriveHandSpeed = 12.0f;
	static constexpr float minOverdriveHandSpeed = -12.0f;
	float speedLimit = normalSpeedLimit;
	static constexpr float normalSpeedLimit = 1.0f;
	static constexpr float overdriveSpeedLimit = 2.0f;
	bool overDriven = false;
	
	Porter por;
};